<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTPHotelImageType extends Model
{
    protected $table = 'htp_image_tags';
}