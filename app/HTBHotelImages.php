<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTBHotelImages extends Model
{
    protected $table = 'htb_hotel_images';
}