<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTBHotels extends Model
{
    protected $table = 'htb_hotels';

    public function images()
    {
        return $this->hasMany('App\HTBHotelImages', 'hotel');
    }

    public function facilities()
    {
        return $this->hasMany('App\HTBHotelFacilities', 'hotel');
    }
}