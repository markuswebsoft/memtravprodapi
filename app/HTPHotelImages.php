<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTPHotelImages extends Model
{
    protected $table = 'htp_images';
}