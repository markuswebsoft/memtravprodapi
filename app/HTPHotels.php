<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTPHotels extends Model
{
    protected $table = 'htp_hotels';

    public function images()
    {
        return $this->hasMany('App\HTPHotelImages', 'hotel');
    }

    public function facilities()
    {
        return $this->hasMany('App\HTPHotelFacilities', 'hotel');
    }
}