<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// the main route will return a list of hotels matching the query and number range

Route::get('/', 'TestController@index');

// this route is to send requests to hotelbeds API


// testing rates


Route::get('/checkRate/{firstRate}', 'RatesController@testCheckRate');

// these routes are dedicated to search for hotels from each network

Route::get('/search/{source}/{apiKey}/{long}/{lat}/{country}/{checkin}/{checkout}/{rooms}/{pax}/{page}','SearchController@index');

// these routes are dedicated to getting the details of a specific hotel

Route::get('/details/htb/{apiKey}/{code}/{long}', 'HotelbedsController@getHotel');
Route::get('/details/htp/{apiKey}/{code}/{long}', 'HotelsproController@getHotel');
Route::get('/details/tapp/{apiKey}/{code}/{long}', 'TAPPController@getHotel');

// these routes are dedicated to getting available rooms of a specific date according to user input

Route::get('/getRates/{source}/{apikey}/{code}/{long}/{lat}/{checkin}/{checkout}/{rooms}{adults}/{children}/{ages}', 'RatesController@index');

// these routes are dedicated to booking a room

// hotel selection
Route::get('/bookhotel/{key}/{hotel}/{checkin}/{checkout}/{occupants}', 'HotelbedsController@getRates');

// room selection
Route::get('/bookmyroom/{apiKey}/{source}/{code}/{rateKey}/{pax}/{long}', 'RatesController@bookRoom');
// secure checkout for TAPP
Route::post('/checkout/{source}', 'RatesController@bookTAPP');

// these routes are dedicated to cancelling a hotel reservation

Route::get('/cancel/htb/{apikey}/{confirm_num}', 'HotelbedsController@cancelReservation');
Route::get('/cancel/htp/{apikey}/{confirm_num}', 'HotelsproController@cancelReservation');

// these routes are dedicated to analytics

// these routes are dedicated to ajax calls

Route::get('/provision/{source}/{apikey}/{code}/{pax}', 'RatesController@provisionAJAX');

Route::get('/bookings/{apikey}','DataController@bookings');

// these routes are dedicated the Chron Jobs

Route::get('/chron/htb', 'ChronController@getHTB');
Route::get('/chron/htp','ChronController@getHTP');

Route::get('/chron/allhtb', 'ChronController@getHTBHotels');
Route::get('/chron/orderhtb','ChronController@orderHTB');
Route::get('/chron/combinehtb','ChronController@combineHTB');
Route::get('/chron/lite','ChronController@makelite');
Route::get('/chron/images','ChronController@getHTBImageTypes');
Route::get('/test', 'TestController@Test');
Route::get('/test/grab', 'TestController@grab');
Route::get('/test/htp', 'TestController@TestHTP');
Route::get('/test/htp/f', 'TestController@TestHTPFaci');
