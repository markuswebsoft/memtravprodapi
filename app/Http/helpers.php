<?php

function paxDecode($pax){
    $paxes = json_decode($pax, true);
    $PAX = array();
    $PAX["adults"] = 0;
    $PAX["children"] = "";
    $PAX["ages"] = "";
    $z = 0;
    
    foreach ($paxes as $room) {
        $PAX["adults"] += $room["adults"]; // number of adults
        if($room["children"] != null){
            $children = explode(',', $room["children"]);
            $PAX["children"] += count($children); // number of children
            if($z == 0){
                $PAX["ages"] =  $PAX["ages"] . $room["children"];
            } else {
                $PAX["ages"] =  $PAX["ages"] . "," . $room["children"];
            }
        }
        $z = $z + 1;
    }
    return $PAX;
}

function roomDecode($pax){
    
    $PAX = array();
    foreach ($paxes as $room) {
        print_r($room);
    }
    
}

function getHTBFile($h){
    switch (true) {
        case ($h > 0 && $h <= 10000):
            return "hotels-10000.json";
            break; 
        case ($h > 10000 && $h <= 20000):
            return "hotels-20000.json";
            break; 
        case ($h > 20000 && $h <= 30000):
            return "hotels-30000.json";
            break; 
        case ($h > 30000 && $h <= 40000):
            return "hotels-40000.json";
            break; 
        case ($h > 40000 && $h <= 50000):
            return "hotels-50000.json";
            break; 
        case ($h > 50000 && $h <= 60000):
            return "hotels-60000.json";
            break; 
        case ($h > 60000 && $h <= 70000):
            return "hotels-70000.json";
            break; 
        case ($h > 70000 && $h <= 80000):
            return "hotels-80000.json";
            break; 
        case ($h > 80000 && $h <= 90000):
            return "hotels-90000.json";
            break; 
        case ($h > 90000 && $h <= 100000):
            return "hotels-100000.json";
            break;
        case ($h > 100000 && $h <= 110000):
            return "hotels-110000.json";
            break; 
        case ($h > 110000 && $h <= 120000):
            return "hotels-120000.json";
            break; 
        case ($h > 120000 && $h <= 130000):
            return "hotels-130000.json";
            break; 
        case ($h > 130000 && $h <= 140000):
            return "hotels-140000.json";
            break; 
        case ($h > 140000 && $h <= 150000):
            return "hotels-150000.json";
            break; 
        case ($h > 150000 && $h <= 160000):
            return "hotels-160000.json";
            break; 
        case ($h > 160000 && $h <= 170000):
            return "hotels-170000.json";
            break; 
        case ($h > 170000 && $h <= 180000):
            return "hotels-180000.json";
            break; 
        case ($h > 180000 && $h <= 190000):
            return "hotels-190000.json";
            break; 
        case ($h > 190000 && $h <= 200000):
            return "hotels-200000.json";
            break; 
        case ($h > 200000 && $h <= 250000):
            return "hotels-250000s.json";
            break;
        case ($h > 250000 && $h <= 300000):
            return "hotels-300000s.json";
            break; 
        case ($h > 300000 && $h <= 350000):
            return "hotels-350000s.json";
            break; 
        case ($h > 350000 && $h <= 400000):
            return "hotels-400000s.json";
            break;
        case ($h > 400000 && $h <= 450000):
            return "hotels-450000s.json";
            break; 
        case ($h > 450000):
            return "hotels-500000s.json";
            break; 
    }
}

function getHTBLite($h){
    switch (true) {
        case ($h > 0 && $h <= 10000):
            return "hotels-lite-10000.json";
            break; 
        case ($h > 10000 && $h <= 20000):
            return "hotels-lite-20000.json";
            break; 
        case ($h > 20000 && $h <= 30000):
            return "hotels-lite-30000.json";
            break; 
        case ($h > 30000 && $h <= 40000):
            return "hotels-lite-40000.json";
            break; 
        case ($h > 40000 && $h <= 50000):
            return "hotels-lite-50000.json";
            break; 
        case ($h > 50000 && $h <= 60000):
            return "hotels-lite-60000.json";
            break; 
        case ($h > 60000 && $h <= 70000):
            return "hotels-lite-70000.json";
            break; 
        case ($h > 70000 && $h <= 80000):
            return "hotels-lite-80000.json";
            break; 
        case ($h > 80000 && $h <= 90000):
            return "hotels-lite-90000.json";
            break; 
        case ($h > 90000 && $h <= 100000):
            return "hotels-lite-100000.json";
            break;
        case ($h > 100000 && $h <= 110000):
            return "hotels-lite-110000.json";
            break; 
        case ($h > 110000 && $h <= 120000):
            return "hotels-lite-120000.json";
            break; 
        case ($h > 120000 && $h <= 130000):
            return "hotels-lite-130000.json";
            break; 
        case ($h > 130000 && $h <= 140000):
            return "hotels-lite-140000.json";
            break; 
        case ($h > 140000 && $h <= 150000):
            return "hotels-lite-150000.json";
            break; 
        case ($h > 150000 && $h <= 160000):
            return "hotels-lite-160000.json";
            break; 
        case ($h > 160000 && $h <= 170000):
            return "hotels-lite-170000.json";
            break; 
        case ($h > 170000 && $h <= 180000):
            return "hotels-lite-180000.json";
            break; 
        case ($h > 180000 && $h <= 190000):
            return "hotels-lite-190000.json";
            break; 
        case ($h > 190000 && $h <= 200000):
            return "hotels-lite-200000.json";
            break; 
        case ($h > 200000 && $h <= 250000):
            return "hotels-lite-250000.json";
            break;
        case ($h > 250000 && $h <= 300000):
            return "hotels-lite-300000.json";
            break; 
        case ($h > 300000 && $h <= 350000):
            return "hotels-lite-350000.json";
            break; 
        case ($h > 350000 && $h <= 400000):
            return "hotels-lite-400000.json";
            break;
        case ($h > 400000 && $h <= 450000):
            return "hotels-lite-450000.json";
            break; 
        case ($h > 450000):
            return "hotels-lite-500000.json";
            break; 
    }
}

function getHTPFile($l){
    switch (true) {
        case  ($l > -180 && $l < -170):
            return "hotels-0.json";
            break;
        case  ($l > -170 && $l < -160):
            return "hotels-1.json";
            break;
        case  ($l > -160 && $l < -150):
            return "hotels-2.json";
            break;
        case  ($l > -150 && $l < -140):
            return "hotels-3.json";
            break;
        case  ($l > -140 && $l < -130):
            return "hotels-4.json";
            break;
        case  ($l > -130 && $l < -120):
            return "hotels-5.json";
            break;
        case  ($l > -120 && $l <= -116.94755):
            return "hotels-6-1.json";
            break;
        case  ($l > -116.94755 && $l < -110):
            return "hotels-6-2.json";
            break;
        case  ($l > -110 && $l < -100):
            return "hotels-7.json";
            break;
        case  ($l > -100 && $l <= -95.61720818):
            return "hotels-8-1.json";
            break;
        case  ($l > -95.61720818 && $l < -90):
            return "hotels-8-2.json";
            break;
        case  ($l > -90 && $l <= -86.723449):
            return "hotels-9-1.json";
            break;
        case  ($l > -86.723449 && $l <= -83.96845087):
            return "hotels-9-2.json";
            break;
        case  ($l > -83.96845087 && $l <= -81.600639):
            return "hotels-9-3.json";
            break;
        case  ($l > -81.600639 && $l < -80):
            return "hotels-9-4.json";
            break;
        case  ($l > -80 && $l <= -77.41080791):
            return "hotels-10-1.json";
            break;
        case  ($l > -77.41080791 && $l <= -75.08503765):
            return "hotels-10-2.json";
            break;
        case  ($l > -75.08503765 && $l <= -72.88930357):
            return "hotels-10-3.json";
            break;
        case  ($l > -72.88930357 && $l < -70):
            return "hotels-10-4.json";
            break;
        case  ($l > -70 && $l < -60):
            return "hotels-11.json";
            break;
        case  ($l > -60 && $l < -50):
            return "hotels-12.json";
            break;
        case  ($l > -50 && $l < -40):
            return "hotels-13.json";
            break;
        case  ($l > -40 && $l < -30):
            return "hotels-14.json";
            break;
        case  ($l > -30 && $l < -20):
            return "hotels-15.json";
            break;
        case  ($l > -20 && $l < -10):
            return "hotels-16.json";
            break;

        case  ($l > -10 && $l <= -7.112142742):
            return "hotels-17-1.json";
            break;
        case  ($l > -7.112142742 && $l <= -4.00816):
            return "hotels-17-2.json";
            break;
        case  ($l > -4.00816 && $l <= -2.584888673):
            return "hotels-17-3.json";
            break;
        case  ($l > -2.584888673 && $l <= -1.0078):
            return "hotels-17-4.json";
            break;
        case  ($l > -1.0078 && $l <= -0.049991012):
            return "hotels-17-5.json";
            break;
        case  ($l > -0.049991012 && $l <= 2.165187746):
            return "hotels-17-6.json";
            break;
        case  ($l > 2.165187746 && $l <= 3.032935281):
            return "hotels-17-7.json";
            break;
        case  ($l > 3.032935281 && $l <= 5.077):
            return "hotels-17-8.json";
            break;
        case  ($l > 5.077 && $l <= 7.174447775):
            return "hotels-17-9.json";
            break;
        case  ($l > 7.174447775 && $l <= 8.688784093):
            return "hotels-17-10.json";
            break;
        case  ($l > 8.688784093 && $l < 10):
            return "hotels-17-11.json";
            break;

        case  ($l > 10 && $l <= 10.98624229):
            return "hotels-18-1.json";
            break;
        case  ($l > 10.98624229 && $l <= 11.76899479):
            return "hotels-18-2.json";
            break;
        case  ($l > 11.76899479 && $l <= 12.46755):
            return "hotels-18-3.json";
            break;
        case  ($l > 12.46755 && $l <= 12.80096591):
            return "hotels-18-4.json";
            break;
        case  ($l > 12.80096591 && $l <= 14.08497959):
            return "hotels-18-5.json";
            break;
        case  ($l > 14.08497959 && $l <= 15.22755235):
            return "hotels-18-6.json";
            break;
        case  ($l > 15.22755235 && $l <= 18.00808311):
            return "hotels-18-7.json";
            break;
        case  ($l > 18.00808311 && $l < 20):
            return "hotels-18-8.json";
            break;
        case  ($l > 20 && $l <= 24.42310721):
            return "hotels-19-1.json";
            break;
        case  ($l > 24.42310721 && $l <= 27.38874704):
            return "hotels-19-2.json";
            break;
        case  ($l > 27.38874704 && $l < 30):
            return "hotels-19-3.json";
            break;
        case  ($l > 30 && $l <= 34.04089898):
            return "hotels-20-1.json";
            break;
        case  ($l > 34.04089898 && $l < 40):
            return "hotels-20-2.json";
            break;
        case  ($l > 40 && $l < 50):
            return "hotels-21.json";
            break;
        case  ($l > 50 && $l < 60):
            return "hotels-22.json";
            break;
        case  ($l > 60 && $l < 70):
            return "hotels-23.json";
            break;
        case  ($l > 70 && $l <= 76.37191594):
            return "hotels-24-1.json";
            break;
        case  ($l > 76.37191594 && $l < 80):
            return "hotels-24-2.json";
            break;
        case  ($l > 80 && $l < 90):
            return "hotels-25.json";
            break;
        case  ($l > 90 && $l <= 98.91041636):
            return "hotels-26-1.json";
            break;
        case  ($l > 98.91041636 && $l < 100):
            return "hotels-26-2.json";
            break;
        case  ($l > 100 && $l <= 100.9099603):
            return "hotels-27-1.json";
            break;
        case  ($l > 100.9099603 && $l <= 103.7685621):
            return "hotels-27-2.json";
            break;
        case  ($l > 103.7685621 && $l <= 106.69271):
            return "hotels-27-3.json";
            break;
        case  ($l > 106.69271 && $l < 110):
            return "hotels-27-4.json";
            break;
        case  ($l > 110 && $l <= 113.57177):
            return "hotels-28-1.json";
            break;
        case  ($l > 113.57177 && $l <= 115.17227):
            return "hotels-28-2.json";
            break;
        case  ($l > 115.17227 && $l <= 116.60009):
            return "hotels-28-3.json";
            break;
        case  ($l > 116.60009 && $l < 120):
            return "hotels-28-4.json";
            break;
        case  ($l > 120 && $l <= 120.80127):
            return "hotels-29-1.json";
            break;
        case  ($l > 120.80127 && $l <= 121.58018):
            return "hotels-29-2.json";
            break;
        case  ($l > 121.58018 && $l <= 126.4976107):
            return "hotels-29-3.json";
            break;
        case  ($l > 126.4976107 && $l < 130):
            return "hotels-29-4.json";
            break;
        case  ($l > 130 && $l < 140):
            return "hotels-30.json";
            break;
        case  ($l > 140 && $l < 150):
            return "hotels-31.json";
            break;
        case  ($l > 150 && $l < 160):
            return "hotels-32.json";
            break;
        case  ($l > 160 && $l < 170):
            return "hotels-33.json";
            break;
    }
}

function rutime($ru, $rus, $index) {
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

function sorter($key){
    return function ($a, $b) use ($key) {
        return $a[$key] - $b[$key];
    };
}
function sortHightoLow($key){
    return function ($a, $b) use ($key) {
        return $b[$key] - $a[$key];
    };
}
//




