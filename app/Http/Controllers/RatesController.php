<?php

namespace App\Http\Controllers;

require base_path() .'/vendor/autoload.php';

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\Countries;
use DB;
use Zend;
use DateTime;


use hotelbeds\hotel_api_sdk\HotelApiClient;
use hotelbeds\hotel_api_sdk\messages\BookingConfirmRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRQ;
use hotelbeds\hotel_api_sdk\model\Geolocation;
use hotelbeds\hotel_api_sdk\model\Hotels;
use hotelbeds\hotel_api_sdk\model\Destination;
use hotelbeds\hotel_api_sdk\model\Occupancy;
use hotelbeds\hotel_api_sdk\model\Pax;
use hotelbeds\hotel_api_sdk\model\Rate;
use hotelbeds\hotel_api_sdk\model\Stay;
use hotelbeds\hotel_api_sdk\types\ApiVersion;
use hotelbeds\hotel_api_sdk\types\ApiVersions;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRS;
use hotelbeds\hotel_api_sdk\messages\BookingListRS;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


class RatesController extends Controller
{
	/**
     * @var HotelApiClient
     */

	private $apiClient;

    public function index($source, $apikey, $code, $long, $lat, $checkin, $checkout, $rooms, $adults, $children, $ages){

    	$client = new Client;

    	if($source == "htb"){
    		// this module only gets room rates from hotelbeds
    		$hotelpath = base_path(). "/vendor/hotelbeds/hotel-api-sdk-php/tests/";
	    	$reader = new Zend\Config\Reader\Ini();
			$config   = $reader->fromFile($hotelpath . 'HotelApiClient.ini');
			$cfgApi = $config["apiclient"];

			$apiClient = new HotelApiClient(getenv('HTB_URL'),
	            getenv('HTB_KEY'),
	            getenv('HTB_SECRET'),
            new ApiVersion(ApiVersions::V1_0), "120" );

			$rqData = new \hotelbeds\hotel_api_sdk\helpers\Availability();
			$rqData->stay = new Stay(DateTime::createFromFormat("Y-m-d", $checkin),
			                         DateTime::createFromFormat("Y-m-d", $checkout));
			$geolocation = new Geolocation();
	        $geolocation->latitude = (double)$lat;
	        $geolocation->longitude= (double)$long;
	        $geolocation->radius= (double)0.5;
	        $geolocation->unit= Geolocation::KM;

	        $rqData->geolocation = $geolocation;

	        //$rqData->hotels = $hotels;
			
			$occupancy = new Occupancy();
		$occupancy->rooms = (int)$rooms;
		$occupancy->children = (int)$children;
		$occupancy->adults = (int)$adults;

		$ages = explode(",", $ages);

		foreach ($ages as $key => $value) {
			$age[$key] = (int)$value;
		}
		if($children > 0){ if($children == 1){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]) ];}if($children == 2){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]) ];}if($children == 3){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]) ];}if($children == 4){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]) ];}if($children == 5){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]) ];}if($children == 6){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]), new Pax(PAX::CH, $age[5]) ];}if($children == 7){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]), new Pax(PAX::CH, $age[5]), new Pax(PAX::CH, $age[6]) ];}if($children == 8){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]), new Pax(PAX::CH, $age[5]), new Pax(PAX::CH, $age[6]), new Pax(PAX::CH, $age[7]) ];}if($children == 9){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]), new Pax(PAX::CH, $age[5]), new Pax(PAX::CH, $age[6]), new Pax(PAX::CH, $age[7]), new Pax(PAX::CH, $age[8]) ];}if($children == 10){$occupancy->paxes = [ new Pax(PAX::CH, $age[0]), new Pax(PAX::CH, $age[1]), new Pax(PAX::CH, $age[2]), new Pax(PAX::CH, $age[3]), new Pax(PAX::CH, $age[4]), new Pax(PAX::CH, $age[5]), new Pax(PAX::CH, $age[6]), new Pax(PAX::CH, $age[7]), new Pax(PAX::CH, $age[8]), new Pax(PAX::CH, $age[9]) ];} }
		$rqData->occupancies = [ $occupancy ];
			


			try {
	    		$availRS = $apiClient->Availability($rqData);
			} catch (\hotelbeds\hotel_api_sdk\types\HotelSDKException $e) {
			    $auditData = $e->getAuditData();
			    error_log( $e->getMessage() );
			    error_log( "Audit remote data = ".json_encode((array)$auditData));
			    exit();
			} catch (Exception $e) {
			    error_log( $e->getMessage() );
			    exit();
			}

			// Check availability is empty or not!
			if (!$availRS->isEmpty()) {
			   $hotelRate = $availRS->hotels->toArray();
        

			   $hotel = [];
			   foreach ($hotelRate["hotels"][0]["rooms"] as $key => $value) {
			   		$hotel[$key] = $value;
			   }
			 	echo json_encode($hotel, 128);
			 	
			} else {
			   echo "There are no results!";
			}
    	}

    	if($source == "htp"){
    		// calculate paxes

    		$numpax = (int)$adults / (int)$rooms;

    		$pax = "?pax=$numpax";

    		for($i = 1; $i < $numpax; $i++){
    			$pax = $pax . "&pax=$numpax";
    		}


	    	$endpoint = getenv('HTP_URL') . "/search/$pax&checkout=$checkout&checkin=$checkin&hotel_code=$code&client_nationality=us&currency=USD";
	    	
	    	$request = new HTTP_Request('GET', $endpoint);

	    	$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $endpoint);
			$HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");
			$resp = curl_exec($ch);
			echo $resp;
    	}

    	if($source == "tapp"){
    		$endpoint = "https://partnerapi.expedia.com/m/hotels/trip/create?productKey=$code&tripTitle=Membership Travel&roomInfoFields=1";
    		//$endpoint = "https://partnerapi.expedia.com/m/hotels/offers?hotelId=$code&checkInDate=$checkin&checkOutDate=$checkout&room=$rooms&format=JSON";
			$TAPP_KEY = "useragent=" . getenv('TAPP_KEY');
			$TAPP_SECRET = "expedia-apikey key=" . getenv('TAPP_SECRET');
			$headers = ["User-Agent" => $TAPP_KEY, "Authorization" => $TAPP_SECRET];
			
			$response = $client->request('GET', $endpoint, ['headers' => $headers]);
			$body = $response->getBody();
			echo $body;
    	}
    }

    // this function produces the data for the checkout page

    public function bookRoom($apiKey, $source, $code, $rateKey, $pax, $long)
    {
    	ini_set('memory_limit','2560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
    	$client = new Client;

    	$adults = paxDecode($pax)["adults"];

    	if($source == "htb"){

    		$hotelpath = base_path(). "/vendor/hotelbeds/hotel-api-sdk-php/tests/";
	    	$reader = new Zend\Config\Reader\Ini();
			$config   = $reader->fromFile($hotelpath . 'HotelApiClient.ini');
			$cfgApi = $config["apiclient"];
    		$apiClient = new HotelApiClient(getenv('HTB_URL'),
	            getenv('HTB_KEY'),
	            getenv('HTB_SECRET'),
            new ApiVersion(ApiVersions::V1_0), "120" );

	        $rqCheck = new \hotelbeds\hotel_api_sdk\helpers\CheckRate();
	        $rqCheck->rooms = [ ["rateKey" => $rateKey] ];

	        $hotel = $apiClient->checkRate($rqCheck)->hotel->toArray();

    		$order = array(
            "hotel" => $hotel["name"],
            "destination" => $hotel["destinationName"],
            "checkIn" => $hotel["checkIn"],
            "checkOut" => $hotel["checkOut"],
            "code" => $hotel["code"],
            "roomName" => $hotel["rooms"][0]["name"],
            "roomCode" => $hotel["rooms"][0]["code"],
            "rateKey" => $hotel["rooms"][0]["rates"][0]["rateKey"],
            "rooms" => $hotel["rooms"][0]["rates"][0]["rooms"],
            "roomPrice" => $hotel["rooms"][0]["rates"][0]["net"],
            "roomTax" => "15",
            "adults" => $adults,
            "children" => 0,
            "boardName" => $hotel["rooms"][0]["rates"][0]["boardName"],
            "rateType" => $hotel["rooms"][0]["rates"][0]["rateType"],
            "rateClass" => $hotel["rooms"][0]["rates"][0]["rateClass"],
            "cancellationPolicy" => $hotel["rooms"][0]["rates"][0]["cancellationPolicies"],
            "currency" => $hotel["currency"]
        );

        echo json_encode($order, 128);
    	}

    	if($source == "htp"){

    		$response = array("hoteldata" => array(), "response" => array());

    		// we need to get hotel data for HTP

    		$public = public_path();
    		$path = $public . "/htp/" . getHTPFile($long);

    		$hotelsDB = json_decode(file_get_contents("$path"), true);

			foreach ($hotelsDB as $hotelD) {
				if($hotelD["code"] == $code){
					$response["hoteldata"] = $hotelD;
				}
			}

    		$endpoint = getenv('HTP_URL') . "/provision/$rateKey";

	    	$ch = curl_init();
			$HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
			curl_setopt($ch, CURLOPT_URL, $endpoint);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");
			$resp = curl_exec($ch);

			$response["response"] = $resp;

			echo json_encode($response, 128);
    	}

    	if($source == "tapp"){
    		$endpoint = "https://partnerapi.expedia.com/m/hotels/trip/create?productKey=$rateKey&tripTitle=Test%20Itin&roomInfoFields=1";
			$headers = ["User-Agent" => 'useragent=bookmyrooms', "Authorization" => 'expedia-apikey key=22f107c4-2145-4062-86b1-0bb3152c77b0'];
			
			$response = $client->request('GET', $endpoint, ['headers' => $headers]);
			$body = $response->getBody();
			echo $body;
    	}
    }

    public function testCheckRate($firstRate)
    {
        $rqCheck = new \hotelbeds\hotel_api_sdk\helpers\CheckRate();
        $rqCheck->rooms = [ ["rateKey" => $firstRate->rateKey] ];

        return $this->apiClient->checkRate($rqCheck);
    }

        public function testBookingConfirm($rateKey, $pax)
    	{
			$rqBookingConfirm = new \hotelbeds\hotel_api_sdk\helpers\Booking();
			$rqBookingConfirm->holder = new \hotelbeds\hotel_api_sdk\model\Holder("Tomeu TEST", "Capo TEST");
			$rqBookingConfirm->language="CAS";

			// Use this iterator for multiple pax distributions, this example have one only pax distribution.

			$children = paxDecode($pax)["children"];
			$adults = paxDecode($pax)["adults"];
			$ages = paxDecode($pax)["ages"];
			$paxing = json_decode($pax, true);

			$guests=array_fill(0, $adults+$children, NULL);
			$pos=0;
			for($i = 1; $i <= count($paxing); $i++){
				for ($x=0;$x<$paxing[$x]["adults"]; $x++){
					if($x == 0){
				    $guests[$pos]= new Pax(Pax::AD, 30, "test","test", $i);
				    $pos++;
					} 
				}
				for ($y=0;$y<$children; $y++) {
				    $guests[$pos]= new Pax(Pax::CH, $ages[$y], "test","test", $i);
				    $pos++;
				}
			}

			$bookRooms = [];
			$atWeb = false;
			$bookingRoom = new \hotelbeds\hotel_api_sdk\model\BookingRoom($rateKey);
			$bookingRoom->paxes = $guests;
			$bookRooms[] = $bookingRoom;

			// Check all bookable rooms are inserted for confirmation.

			$rqBookingConfirm->rooms = $bookRooms;

			// Define payment data for booking confirmation
			$rqBookingConfirm->clientReference = "PHP_TEST_2";


           try {
           	$hotelpath = base_path(). "/vendor/hotelbeds/hotel-api-sdk-php/tests/";
        $reader = new Zend\Config\Reader\Ini();
        $config   = $reader->fromFile($hotelpath . 'HotelApiClient.ini');
        $cfgApi = $config["apiclient"];
        $apiClient = new HotelApiClient(
            "https://api.hotelbeds.com",
	            "6dkxckesy858wcghpmdcw3sd",
	            "FVfNRnnGwn",
            new ApiVersion(ApiVersions::V1_0),
            $cfgApi["timeout"]
        );
               $confirmRS = $apiClient->BookingConfirm($rqBookingConfirm);
               return $confirmRS;
           } catch (\hotelbeds\hotel_api_sdk\types\HotelSDKException $e) {
               echo "\n".$e->getMessage()."\n";
               echo "\n".$apiClient->getLastRequest()->getContent()."\n";
               $this->fail($e->getMessage());
           }

           return null;
    }
    public function bookTAPP(Request $request){
    	$input = $request->all();
    	$response = [];
    	foreach ($input as $key => $value) {
    		$response[$key] = $value;
    	}

      $zip = $request->zip;
      $checkin = $request->checkin;
      $checkout = $request->checkout;
      $cardtype = $request->cardtype;
      $code = $request->code;
      $cardnum = $request->cardnum;
      $mm = $request->mm;
      $yy = $request->yy;
      $exp = "$yy-$mm";
      $cvv = $request->cvv;
      $firstname = $request->firstname;
      $lastname = $request->lastname;
      $fare = $request->fare;
      $cardname = "++$firstname+$lastname";
      $phone = $request->phone;
      $phonecode = $request->phonecode;
      $email = $request->email;

      $endpoint = "https://partnerapi.expedia.com/m/hotels/trip/checkout?checkInDate=$checkin&checkOutDate=$checkout&creditCardNumber=$cardnum&cvv=$cvv&email=$email&expirationDateYear=$yy&expirationDateMonth=$mm&firstName=$firstname&lastName=$lastname&phone=$phone&phoneCountryCode=$phonecode&postalCode=$zip&sendEmailConfirmation=FALSE&country=USA&nameOnCard=++$firstname+$lastname&prettyPrint=true&supressFinalBooking=true&tripId=$code&expectedTotalFare=$fare&expectedFareCurrencyCode=USD";
		echo $endpoint;
      $headers = ["User-Agent" => 'useragent=bookmyrooms', "Authorization" => 'expedia-apikey key=22f107c4-2145-4062-86b1-0bb3152c77b0'];
      $client = new Client;
      $response = $client->request('GET', $endpoint, ['headers' => $headers]);
      $body = $response->getBody();
      echo $body;
      
    }

    public function provisionAJAX($source, $apiKey, $code, $pax){
    	if($source == "htb"){
    		$hotelpath = base_path(). "/vendor/hotelbeds/hotel-api-sdk-php/tests/";
	    	$reader = new Zend\Config\Reader\Ini();
			$config   = $reader->fromFile($hotelpath . 'HotelApiClient.ini');
			$cfgApi = $config["apiclient"];
    		$apiClient = new HotelApiClient(getenv('HTB_URL'),
	            getenv('HTB_KEY'),
	            getenv('HTB_SECRET'),
            new ApiVersion(ApiVersions::V1_0), "120" );

	        $rqCheck = new \hotelbeds\hotel_api_sdk\helpers\CheckRate();
	        $rqCheck->rooms = [ ["rateKey" => $code] ];

	        $hotel = $apiClient->checkRate($rqCheck)->hotel->toArray();

	        if(!array_key_exists("checkIn", $hotel)){
	        	return '{"error_code": "4410", "detail": "The room is no longer available"}';
	        }
    	}
    	if($source == "htp"){
    		$endpoint = getenv('HTP_URL') . "/provision/$code";

	    	$ch = curl_init();
			$HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
			curl_setopt($ch, CURLOPT_URL, $endpoint);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");
			$resp = curl_exec($ch);

			echo json_encode($resp, 128);
    	}
    }
}
