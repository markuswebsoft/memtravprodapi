<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\HTPHotels;
use App\HTPHotelImages;
use App\HTPHotelFacilities;
use App\HTPHotelFacilityType;
use App\HTPHotelImageType;

class HotelsproController extends Controller
{
	public function index($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax){
		ini_set('memory_limit','2560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
		$response = array();
		$public = public_path();
		// paxes
		$adults = paxDecode($pax)["adults"];
		$paxes = json_decode($pax, true);
		// occupants per room 
		$z = 0;
		foreach ($paxes as $room) {
			if($z == 0){
				$pax = "?pax=" . (int)$room["adults"];
				if($room["children"] != null){
					$pax = $pax . "," . $room["children"];
				}
			} else {
				$pax = $pax . "&pax=" . $room["adults"];
				if($room["children"] != null){
					$pax = $pax . "," . $room["children"];
				}
			}
			$z = $z + 1;
		}

		$ch = curl_init();
		$endpoint = getenv('HTP_URL') . "/search/$pax&checkout=$checkout&checkin=$checkin&lat=$lat&lon=$long&radius=20000&client_nationality=us&currency=USD";
	    $HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
	    curl_setopt($ch, CURLOPT_URL,$endpoint);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");

		$hotels = json_decode(curl_exec($ch), true);

		foreach ($hotels["results"] as $hotel) {

			$code = $hotel["hotel_code"];
			$images = array();
			$rooms = array("name" => array(), "price" => array(),"board" => array(),"key" => array(),"quantity" => array());

			foreach ($hotel["products"] as $room) {
				array_push($rooms["price"], $room["price"]);
				array_push($rooms["name"], $room["rooms"][0]["room_description"]);
				array_push($rooms["key"], $room["code"]);
				array_push($rooms["board"], $room["meal_type"]);
				array_push($rooms["quantity"], count($room["rooms"]));
			}

			if($room["pay_at_hotel"] == "" || $room["pay_at_hotel"] == null || $room["pay_at_hotel"] == false){

				// we need to get the hotel details 


			   	$HTPHotel = \App\HTPHotels::where('code', $code)->first();


                $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
						if($HTPHotel->description != ""){
								$description = $HTPHotel->description;
							
						}
						$images = $hoteldata["images"][0];
						
							$results = array(
							
							"name" => $HTPHotel->hotel,
							"description" => $description,
							"code" => $code,
							"country" => $HTPHotel->country,
							"latitude" => $HTPHotel->latitude,
							"longitude" => $HTPHotel->longitude,
							"stars" => $HTPHotel->stars,
							"images" => $images,
							"rooms" => $rooms,
							"source" => "htp"
							);

							array_push($response, $results);
						

			}
		}
		echo json_encode($response);

/*
		$endpoint = "";
		$request = new HTTP_Request('GET', $endpoint);
		$response = array();
		$public = public_path();

		ini_set('memory_limit','2560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);

		// Script start

		$hotelsDB = file_get_contents("$public/db/hotels.json");
		$hotels = json_decode($hotelsDB, true); 

		
	    		
		$i = 0;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "BookmyRoomsUs:L44PxusZla9242h");


		
		foreach ($hotels as $hotel) {
			$hotelLat = $hotel["latitude"];
			$hotelLong = $hotel["longitude"];
			$upLong = $long + 1;
			$downLong = $long - 1;
			$upLat = $lat + 1;
			$downLat = $lat - 1;

			if(((float)$hotelLong <= (float)$upLong && (float)$hotelLong >= (float)$downLong) && ( (float)$hotelLat <= (float)$upLat && (float)$hotelLat >= (float)$downLat)){

				$images = array();
				$rooms = array();
				$rooms["name"] = array();
				$rooms["price"] = array();
				$rooms["board"] = array();
				$rooms["key"] = array();
				$rooms["quantity"] = array();

				foreach ((array)$hotel["images"] as $image) {
					array_push($images, $image["original"]);
					break;
				}

				// we need to get the rooms for each hotel

				
	    		$hotelcode = $hotel["code"];

		    	$endpoint = "https://api2.hotelspro.com/api/v2/search/$pax&checkout=$checkout&checkin=$checkin&hotel_code=$hotelcode&client_nationality=US&currency=USD";

		    	
				curl_setopt($ch, CURLOPT_URL, $endpoint);
				$time_start = microtime(true); 
				$resp = curl_exec($ch);
				$time_end = microtime(true);
				if(strlen($resp) > 100){
				
					$roomsresponse = json_decode($resp, true);


					if(array_key_exists("0", $roomsresponse["results"])){
					foreach ($roomsresponse["results"][0]["products"] as $room) {
						array_push($rooms["price"], $room["price"]);
						array_push($rooms["name"], $room["rooms"][0]["room_description"]);
						array_push($rooms["key"], $room["code"]);
						array_push($rooms["board"], $room["meal_type"]);
						array_push($rooms["quantity"], count($room["rooms"]));
						

						}
					}
					if($room["pay_at_hotel"] == "" || $room["pay_at_hotel"] == null || $room["pay_at_hotel"] == false){
					$results = array(
						"name" => $hotel["name"],
						"description" => $hotel["descriptions"]["hotel_information"],
						"code" => $hotel["code"],
						"country" => $hotel["country"],
						"latitude" => $hotel["latitude"],
						"longitude" => $hotel["longitude"],
						"stars" => $hotel["stars"],
						"images" => $images,
						"rooms" => $rooms,
						"source" => "htp"
						);
					array_push($response, $results);
					$i = $i + 1;
					if($i == 1){
						break;
					}
				}
			}
		}
	}
	
	//dividing with 60 will give the execution time in minutes other wise seconds
$execution_time = ($time_end - $time_start);

//execution time of the script
echo '<b>Total Execution Time:</b> '.$execution_time.' Seconds <br><br><br><br>';


	echo json_encode($response, 128);


*/
	}

    public function getHotel($apiKey, $code, $long){

		$images = array(); 
		$images["name"] = array();
		$images["path"] = array();
		$facilities = array();
		$facilities["payments"] = array();
		$facilities["room_amenities"] = array();
		$facilities["hotel_amenities"] = array();  

		// open the database
        $HTPHotel = \App\HTPHotels::where('code', $code)->first();
        $HTPImages = \App\HTPHotelImages::where('hotel', $code)->get();
        $HTPFacilities = \App\HTPHotelFacilities::where('hotel', $code)->get();


        $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
        if($HTPHotel->description != ""){
            $description = $HTPHotel->description;
        }

		foreach ($HTPImages as $image ){
            $HTPImageType = \App\HTPHotelImageType::where('code', $image->category)->first();
            if(!empty($HTPImageType)){
                array_push($images["path"], $image->original);

                array_push($images["name"], $HTPImageType->name);
            }

		}

		foreach ($HTPFacilities as $facility) {
		        $HTPFacilityType = \App\HTPHotelFacilityType::where('code', $facility->facilityCode)->first();
                array_push($facilities["hotel_amenities"],$HTPFacilityType->facilityName);
		}

		// now we stuff the final response
		$result = array(
                	"name" => $HTPHotel->hotel,
                	"address" => $HTPHotel->address,
                	"city" => null,
                	"country" => $HTPHotel->country,
                	"description" => $description,
                	"phonenumbers" => null,
                	"rooms" => null,
                	"images" => $images,
                	"facilities" => $facilities,
                	"stars" => $HTPHotel->stars
                	);

		echo json_encode($result, 128);

    }

    public function cancelReservation($apiKey, $confirm_num){
    	$ch = curl_init();
		$HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");
		$endpoint = getenv('HTP_URL') . "/cancel/$confirm_num";

		curl_setopt($ch, CURLOPT_URL, $endpoint);
		$resp = curl_exec($ch);

    	echo $resp;
    }
}
