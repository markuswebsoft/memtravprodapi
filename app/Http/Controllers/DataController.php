<?php

namespace App\Http\Controllers;

require base_path() .'/vendor/autoload.php';

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;

use hotelbeds\hotel_api_sdk\HotelApiClient;
use hotelbeds\hotel_api_sdk\messages\BookingConfirmRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRQ;
use hotelbeds\hotel_api_sdk\model\Geolocation;
use hotelbeds\hotel_api_sdk\model\Hotels;
use hotelbeds\hotel_api_sdk\model\Destination;
use hotelbeds\hotel_api_sdk\model\Occupancy;
use hotelbeds\hotel_api_sdk\model\Pax;
use hotelbeds\hotel_api_sdk\model\Rate;
use hotelbeds\hotel_api_sdk\model\Stay;
use hotelbeds\hotel_api_sdk\types\ApiVersion;
use hotelbeds\hotel_api_sdk\types\ApiVersions;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRS;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRQ;
use hotelbeds\hotel_api_sdk\messages\BookingListRS;
use hotelbeds\hotel_api_sdk\messages\BookingListRQ;
use hotelbeds\hotel_api_sdk\messages\BookingCancellationRS;

class DataController extends Controller
{
	/**
     * @var HotelApiClient
     */

	private $apiClient;
	private $pax;
	private $occupancy;

    public function bookings($apiKey){
    	$this->apiClient = new HotelApiClient("https://api.hotelbeds.com",
	            "6dkxckesy858wcghpmdcw3sd",
	            "FVfNRnnGwn",
        new ApiVersion(ApiVersions::V1_0), "120" );

        $bkListRS = $this->testBookingList();
        $list = $this->testBookingListRS($bkListRS);

        print_r($list);
    }

    public function testBookingList()
    {
        $rqBookingLst = new \hotelbeds\hotel_api_sdk\helpers\BookingList();
        $rqBookingLst->start = DateTime::createFromFormat("Y-m-d", "2016-11-01");
        $rqBookingLst->end = DateTime::createFromFormat("Y-m-d", "2016-11-30");
        $rqBookingLst->from = 1;
        $rqBookingLst->to = 25;
        return $this->apiClient->bookinglist($rqBookingLst);
    }

    /**
     * @depends testBookingList
     */

    public function testBookingListRS($bkListRS)
    {
        $firstBooking = null;
        // Check is response is empty or not
        if($bkListRS->isEmpty()){ echo "Booking list is empty!"; }
        foreach ($bkListRS->bookings->iterator() as $reference => $booking)
        {
            if ($booking->status === "CONFIRMED") {
                print_r($booking);
                $firstBooking = $booking->reference;
            }
        }

        return $firstBooking;
    }
}
