<?php

namespace App\Http\Controllers;

require base_path() .'/vendor/autoload.php';

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\Countries;
use DB;
use Zend;
use DateTime;
use App\HTBHotels;
use App\HTBHotelImages;


use hotelbeds\hotel_api_sdk\HotelApiClient;
use hotelbeds\hotel_api_sdk\model\Geolocation;
use hotelbeds\hotel_api_sdk\model\Hotels;
use hotelbeds\hotel_api_sdk\model\Destination;
use hotelbeds\hotel_api_sdk\model\Occupancy;
use hotelbeds\hotel_api_sdk\model\Pax;
use hotelbeds\hotel_api_sdk\model\Rate;
use hotelbeds\hotel_api_sdk\model\Stay;
use hotelbeds\hotel_api_sdk\types\ApiVersion;
use hotelbeds\hotel_api_sdk\types\ApiVersions;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRS;




class TestController extends Controller
{
	private $apiClient;

  public function index(){
    	echo "Not Authorized"; 	
	}
  public function test(){
      ini_set('memory_limit','99999M');
      ini_set('max_execution_time', 90000);
      set_time_limit(90000);

      $dir = public_path() . "/htb";
      $files = scandir($dir);
      print_r($files);
      $x = 0;
      for($i = 0; $i < sizeof($files); $i++){
            if($i > 3 && $i < 30){
                echo "<br>$files[$i] <br>";
                $hotels = json_decode(file_get_contents($dir . "/$files[$i]"), true);
                foreach ($hotels as $hotel) {
                    if($x > 104927){
                    // insert the hotel into the database
                    // we need some logic because the hotelbeds format is inconsistent
                    // PCM = Please contact reservations@membershiptravel.com for more information about this hotel
                    // RMT = reservations@membershiptravel.com

                    if (array_key_exists("description", $hotel)) {
                        $description = $hotel["description"]["content"];
                    } else {
                        $description = "PCM";
                    }
                    if (array_key_exists("email", $hotel)) {
                        $email = $hotel["email"];
                    } else {
                        $email = "RMT";
                    }
                    if (array_key_exists("postalCode", $hotel)) {
                        $postalCode = $hotel["postalCode"];
                    } else {
                        $postalCode = "";
                    }
                    if (array_key_exists("coordinates", $hotel)) {
                        $longitude = $hotel["coordinates"]["longitude"];
                        $latitude = $hotel["coordinates"]["latitude"];
                    } else {
                        $longitude = 0;
                        $latitude = 0;
                    }
                    if (array_key_exists("country", $hotel)) {
                        $country = $hotel["country"]["code"];
                    } else {
                        $country = "";
                    }
                    if (array_key_exists("category", $hotel)) {
                        $category = $hotel["category"]["description"]["content"];
                    } else {
                        $category = "";
                    }
                    //

                    DB::table('htb_hotels')->insert([
                        'code' => $hotel["code"],
                        'hotel' => $hotel["name"]["content"],
                        'description' => $description,
                        'country' => $country,
                        'longitude' => $longitude,
                        'latitude' => $latitude,
                        'stars' => $category,
                        'address' => $hotel["address"]["content"],
                        'city' => $hotel["city"]["content"],
                        'postalCode' => $postalCode
                    ]);
                    // insert email
                    DB::table('htb_phones')->insert([
                        'hotel' => $hotel["code"],
                        'phoneNumber' => $email,
                        'phoneType' => "email"
                    ]);
                    if (array_key_exists("phones", $hotel)) {
                        foreach ($hotel["phones"] as $phone) {
                            DB::table('htb_phones')->insert([
                                'hotel' => $hotel["code"],
                                'phoneNumber' => $phone["phoneNumber"],
                                'phoneType' => $phone["phoneType"]
                            ]);
                        }
                    }
                    if (array_key_exists("boards", $hotel)) {
                        foreach ($hotel["boards"] as $board) {
                            DB::table('htb_board')->insert([
                                'hotel' => $hotel["code"],
                                'board' => $board["code"]
                            ]);
                        }
                    }
                    if (array_key_exists("rooms", $hotel)) {

                        foreach ($hotel["rooms"] as $room) {
                            if(array_key_exists("description", $room)){
                                $roomDescription = $room["description"];
                            } else {
                                $roomDescription = NULL;
                            }
                            DB::table('htb_rooms')->insert([
                                'hotel' => $hotel["code"],
                                'roomCode' => $room["roomCode"],
                                'description' => $roomDescription
                            ]);
                        }
                    }

                }
                    $x = $x + 1;
                    /*
                    if(HTBHotels::where('code', $hotel["code"])->count() == 0) {

                        DB::table('htb_hotels')->insert([
                            'code' => $hotel["code"],
                            'hotel' => $hotel["name"]["content"],
                            'description' => $description,
                            'country' => $country,
                            'longitude' => $longitude,
                            'latitude' => $latitude,
                            'stars' => $category,
                            'address' => $hotel["address"]["content"],
                            'city' => $hotel["city"]["content"],
                            'postalCode' => $postalCode,
                            'email' => $email
                        ]);

                    }

                    // insert the rooms into the database

                    // insert hotel facilities into the database


                    if (array_key_exists("facilities", $hotel)) {
                        $facilities = $hotel["facilities"];
                        foreach ($facilities as $facility) {
                            if (array_key_exists("description", $facility)) {

                                if(array_key_exists("order", $facility)){
                                    $facilityOrder = $facility["order"];
                                } else {
                                    $facilityOrder = null;
                                }

                                if(array_key_exists("facilityCode", $facility)){
                                    $facilityCode = $facility["facilityCode"];
                                } else {
                                    $facilityCode = null;
                                }

                                if(array_key_exists("facilityGroupCode", $facility)){
                                    $facilityGroupCode = $facility["facilityGroupCode"];
                                } else {
                                    $facilityGroupCode = null;
                                }

                                if(array_key_exists("indFee", $facility)){
                                    $indFee = $facility["indFee"];
                                } else {
                                    $indFee = null;
                                }

                                if(array_key_exists("indYesOrNo", $facility)){
                                    $indYesOrNo = $facility["indYesOrNo"];
                                } else {
                                    $indYesOrNo = null;
                                }


                                DB::table('htb_hotel_facilities')->insert([
                                    'hotel' => $hotel["code"],
                                    'order' => $facilityOrder,
                                    'facilityCode' => $facilityCode,
                                    'facilityGroupCode' => $facilityGroupCode,
                                    'facility' => $facility["description"]["content"],
                                    'indFee' => $indFee,
                                    'indYesOrNo' => $indYesOrNo
                                ]);
                            }
                        }
                    }
                    // insert hotel images into database
                    /*
                                        if(array_key_exists("images", $hotel)){
                                            $images = $hotel["images"];
                                            foreach($images as $image) {
                                                if(array_key_exists("type", $image)) {
                                                    if (array_key_exists("description", $image["type"])) {
                                                        $imagename = $image["type"]["description"]["content"];
                                                    } else {
                                                        $imagename = "General View";
                                                    }
                                                } else {
                                                    $imagename = "General View";
                                                }

                                                    DB::table('htb_images')->insert([
                                                        'hotel' => $hotel["code"],
                                                        'order' => $image["order"],
                                                        'path' => "https://photos.hotelbeds.com/giata/" . $image["path"],
                                                        'name' => $imagename
                                                    ]);

                                            }
                                        }
                                        */
                    echo $hotel["code"] . "<br>";
                }
            }
      }
  }
    public function grab(){
        ini_set('memory_limit','99999M');
        ini_set('max_execution_time', 90000);
        set_time_limit(90000);

        $images = \App\HTBHotelImages::where('id', '>', 253882)->get();
        foreach ($images as $image){
            $id = $image->id;
            $path = str_replace("https://photos.hotelbeds.com/giata/","", $image->path);
            DB::table('htb_hotel_images')->where('id', $id)->update(array('path' => $path));
        }
    }

    public function TestHTP(){
        ini_set('memory_limit','99999M');
        ini_set('max_execution_time', 90000);
        set_time_limit(90000);

        $dir = public_path() . "/htp/raw";
        $files = scandir($dir);
        $x = 1;
        for($i = 2; $i < sizeof($files); $i++){
                echo "$files[$i] <br>";
                $hotels = json_decode(file_get_contents($dir . "/$files[$i]"), true);
                foreach ($hotels as $hotel) {
                    if($x > 302670) {
                    if (array_key_exists("descriptions", $hotel)) {
                        if ($hotel["descriptions"] != null) {
                            if (array_key_exists("hotel_information", $hotel["descriptions"])) {
                                $description = $hotel["descriptions"]["hotel_information"];
                            }
                            if (empty($description) && array_key_exists("location_information", $hotel["descriptions"])) {
                                $description = $hotel["descriptions"]["hotel_information"];
                            }
                            if (empty($description)) {
                                echo "<pre>";
                                print_r($hotel["descriptions"]);
                                echo "</pre> missing->" . $hotel["code"];
                                $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
                            }
                        } else {
                            $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
                        }
                    } else {
                        $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
                    }

                    DB::table('htp_hotels')->insert([
                        'code' => $hotel["code"],
                        'hotel' => $hotel["name"],
                        'description' => $description,
                        'country' => $hotel["country"],
                        'longitude' => $hotel["longitude"],
                        'latitude' => $hotel["latitude"],
                        'stars' => $hotel["stars"],
                        'address' => $hotel["address"],
                        'zipcode' => $hotel["zipcode"],
                        'currency' => $hotel["currencycode"],
                        'rooms' => $hotel["nr_rooms"],
                        'checkin' => $hotel["checkin_from"],
                        'checkout' => $hotel["checkout_to"]
                    ]);
                    // insert images
                    if (array_key_exists("images", $hotel)) {
                        foreach ((array)$hotel["images"] as $image) {
                            DB::table('htp_images')->insert([
                                'hotel' => $hotel["code"],
                                'category' => $image["category"],
                                'tag' => $image["tag"],
                                'original' => $image["original"],
                                'small' => $image["thumbnail_images"]["small"],
                                'mid' => $image["thumbnail_images"]["mid"],
                                'large' => $image["thumbnail_images"]["large"]
                            ]);
                        }
                    }
                    // insert facilities
                    if (array_key_exists("facilities", $hotel)) {
                        if (is_array($hotel["facilities"])) {
                            foreach ($hotel["facilities"] as $facility) {
                                DB::table('htp_facilities')->insert([
                                    'hotel' => $hotel["code"],
                                    'facilityCode' => $facility,
                                ]);
                            }
                        }
                    }
                        echo "$x<br>";
                }
                    $x = $x + 1;

            }

        }
    }
    public function TestHTPFaci(){
        $faci = public_path() . "/db/facilities.json";
        echo $faci;
        $facis = json_decode($faci, true);
        print_r($facis);
        foreach ($facis as $facility){
            $code = $facility["code"];
            $name = $facility["name"];
            echo "INSERT INTO htp_facility_types (code, name) VALUES (\"$code\",\"$name\") <br>" ;
        }
    }
}
