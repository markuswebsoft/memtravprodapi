<?php

namespace App\Http\Controllers;

require base_path() .'/vendor/autoload.php';

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;



use hotelbeds\hotel_api_sdk\HotelApiClient;
use hotelbeds\hotel_api_sdk\messages\BookingConfirmRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRQ;
use hotelbeds\hotel_api_sdk\model\Geolocation;
use hotelbeds\hotel_api_sdk\model\Hotels;
use hotelbeds\hotel_api_sdk\model\Destination;
use hotelbeds\hotel_api_sdk\model\Occupancy;
use hotelbeds\hotel_api_sdk\model\Pax;
use hotelbeds\hotel_api_sdk\model\Rate;
use hotelbeds\hotel_api_sdk\model\Stay;
use hotelbeds\hotel_api_sdk\types\ApiVersion;
use hotelbeds\hotel_api_sdk\types\ApiVersions;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRS;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRQ;
use hotelbeds\hotel_api_sdk\messages\BookingListRS;
use hotelbeds\hotel_api_sdk\messages\BookingCancellationRS;

// use these for static content

use App\HTBHotels;
use App\HTPHotels;
use App\HTBHotelImages;
use App\HTPHotelImages;
use App\HTBHotelFacilities;
use App\HTPHotelFacilities;

class SearchController extends Controller
{
    /**
     * @var HotelApiClient
     */

	private $apiClient;
	private $pax;
	private $occupancy;

	public function index($source, $apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax, $page){
        $Hotels = array();
		
		$children = paxDecode($pax)["children"];

		if($rooms == 1 && $children == 0){
			$HTB = $this->getHTB($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax, $page);
			$HTP = $this->getHTP($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax, $page);
			//$TAPP = $this->getTAPP($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax);
			
			if(is_array($HTB) && is_array($HTP)){
				$Hotels = array_merge($HTB, $HTP);
				//if(is_array($TAPP)){
				//	$Hotels = array_merge($Hotels, $TAPP);
				//}
			}
			
		}	
		else {
			$Hotels = $this->getHTP($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax);
		}

		if(is_array($Hotels)){
			usort($Hotels, sortHightoLow('stars'));
			echo json_encode($Hotels);		
		}
	}
	
	public function getHTB($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax, $page){

		$responsedata = array();

		 $this->apiClient = new HotelApiClient(getenv('HTB_URL'),
	            getenv('HTB_KEY'),
	            getenv('HTB_SECRET'),
             new ApiVersion(ApiVersions::V1_0), "120" );

		$availRS = $this->testAvailRQ($long, $lat, $checkin, $checkout, $rooms, $pax);
		$firstRate = $this->testAvailRS($availRS);
		if($firstRate != null){
            $hotelrate = $availRS->hotels->toArray()["hotels"];

            foreach ($hotelrate as $hotel) {
                $rooms = array("name" => array(), "price" => array(), "board" => array(), "key" => array(), "quantity" => array());
                $hotelcode = $hotel["code"];
                $description = "Please contact reservations@membershiptravel.com for more information about this hotel";

                foreach ($hotel["rooms"] as $room) {
                    array_push($rooms["quantity"], $room["rates"]["0"]["rooms"]);
                    array_push($rooms["name"], $room["name"]);
                    array_push($rooms["price"], $room["rates"]["0"]["net"]);
                    array_push($rooms["board"], $room["rates"]["0"]["boardCode"]);
                    array_push($rooms["key"], $room["rates"]["0"]["rateKey"]);
                }

                $htb = DB::table('htb_hotels')
                    ->join('htb_hotel_images', function($join) use ($hotelcode){
                        $join->on('htb_hotels.code', '=', 'htb_hotel_images.hotel')
                            ->where('htb_hotel_images.hotel', '=', $hotelcode);
                    })
                    ->select('htb_hotels.*','htb_hotel_images.path')
                    ->first();

                if(is_object($htb)) {

                    if($htb->description != ""){
                        $description = $htb->description;
                    }

                    $results = array(
                        "name" => $htb->hotel,
                        "description" => $description,
                        "code" => $htb->code,
                        "address" => $htb->address,
                        "city" => $htb->city,
                        "country" => $htb->country,
                        "latitude" => $htb->latitude,
                        "longitude" => $htb->longitude,
                        "stars" => $htb->stars,
                        "images" => "https://photos.hotelbeds.com/giata/" . $htb->path,
                        "rooms" => $rooms,
                        "source" => "htb"
                    );

                    array_push($responsedata, $results);

                }
		    }
		return $responsedata;
		}
	}

	public function testAvailRQ($long, $lat, $checkin, $checkout, $rooms, $pax)
    {
        $rqData = new \hotelbeds\hotel_api_sdk\helpers\Availability();
		$rqData->stay = new Stay(DateTime::createFromFormat("Y-m-d", $checkin),
		                         DateTime::createFromFormat("Y-m-d", $checkout));

        //$rqData->destination = new Destination("PMI");
        $geolocation = new Geolocation();
        $geolocation->latitude = (double)$lat;
        $geolocation->longitude= (double)$long;
        $geolocation->radius= 24.0;
        $geolocation->unit= Geolocation::KM;

        $rqData->geolocation = $geolocation;

        $children = paxDecode($pax)["children"];
        $adults = paxDecode($pax)["adults"];
        $ages = paxDecode($pax)["ages"];
            
        $occupancy = new Occupancy();
        $occupancy->adults = (int)$adults;
        $occupancy->children = (int)$children;
        $occupancy->rooms = (int)$rooms;

        $guests=array_fill(0, $occupancy->adults+$occupancy->children, NULL);
		$pos=0;
		for ($x=0;$x<$occupancy->adults; $x++){
		    $guests[$pos]= new Pax(Pax::AD, 30);
		    $pos++;
		}
		for ($y=0;$y<$occupancy->children; $y++) {
		    $guests[$pos]= new Pax(Pax::CH, $ages[$y]);
		    $pos++;
		}
		$occupancy->paxes =$guests;
		$rqData->occupancies = [ $occupancy ];

        return $this->apiClient->Availability($rqData);
    }

        public function testAvailRS($availRS)
    {
        // Check is response is empty or not

        // Check some fields of response
        // Iterate response hotels, rooms, rates...
        if(!empty($availRS)){
	        foreach ($availRS->hotels->iterator() as $hotelCode => $hotelData)
	        {
	            $hotelData->name;

	            foreach ($hotelData->iterator() as $roomCode => $roomData)
	            {
	                $roomData->code;

	                foreach($roomData->rateIterator() as $rateKey => $rateData)
	                {
	                    $firstRate = $rateData;

	                    $rateData->net;
	                    $rateData->allotment;
	                    $rateData->boardCode;

	                    // Check cancellation policies
	                    foreach($rateData->cancellationPoliciesIterator() as $policyKey => $policyData)
	                    {
	                        $policyData->amount;
	                        $policyData->from;
	                    }

	                    // Check taxes
	                    $taxes = $rateData->getTaxes();
	                    foreach($taxes->iterator() as $tax)
	                    {
	                            //print_r($tax);
	                            //$availRS->tax->type);
	                    }

	                    // Promotions
	                    foreach($rateData->promotionsIterator() as $promoCode => $promoData )
	                    {
	                        $promoData->name;
	                    }
	                }

	            }
	        }
    	}
    	if(isset($firstRate)){
        	return $firstRate;
    	} else {
    		return null;
    	}
    }

    public function testCheckRate($firstRate)
    {
        $rqCheck = new \hotelbeds\hotel_api_sdk\helpers\CheckRate();
        $rqCheck->rooms = [ ["rateKey" => $firstRate->rateKey] ];

        return $this->apiClient->checkRate($rqCheck);
    }

        /**
     * @depends testCheckRate
     */


    public function getHTP($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax, $page){
		$response = array();
		// paxes
		$adults = paxDecode($pax)["adults"];
		$paxes = json_decode($pax, true);
		// occupants per room 
		$z = 0;
		foreach ($paxes as $room) {
			if($z == 0){
				$pax = "?pax=" . (int)$room["adults"];
				if($room["children"] != null){
					$pax = $pax . "," . $room["children"];
				}
			} else {
				$pax = $pax . "&pax=" . $room["adults"];
				if($room["children"] != null){
					$pax = $pax . "," . $room["children"];
				}
			}
			$z = $z + 1;
		}

		$ch = curl_init();
		$endpoint = getenv('HTP_URL') . "/search/$pax&checkout=$checkout&checkin=$checkin&lat=$lat&lon=$long&radius=16000&client_nationality=us&currency=USD";
	    $HTP_LOGIN = getenv('HTP_KEY') . ":" . getenv('HTP_SECRET');
	    curl_setopt($ch, CURLOPT_URL,$endpoint);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_USERPWD, "$HTP_LOGIN");

		$hotels = json_decode(curl_exec($ch), true);

        $start = $page * 25;
        //$hotels = array_slice($hotels, $start, 25, true);

        if(array_key_exists("results", $hotels)) {
            foreach ($hotels["results"] as $hotel) {

                $hotelcode = $hotel["hotel_code"];
                $description = "Please contact reservations@membershiptravel.com for more information about this hotel";
                $rooms = array("name" => array(), "price" => array(), "board" => array(), "key" => array(), "quantity" => array());

                foreach ($hotel["products"] as $room) {
                    array_push($rooms["price"], $room["price"]);
                    array_push($rooms["name"], $room["rooms"][0]["room_description"]);
                    array_push($rooms["key"], $room["code"]);
                    array_push($rooms["board"], $room["meal_type"]);
                    array_push($rooms["quantity"], count($room["rooms"]));
                }

                if ($room["pay_at_hotel"] == "" || $room["pay_at_hotel"] == null || $room["pay_at_hotel"] == false) {

                    $htp = DB::table('htp_hotels')
                        ->join('htp_images', function ($join) use ($hotelcode) {
                            $join->on('htp_hotels.code', '=', 'htp_images.hotel')
                                ->where('htp_images.hotel', '=', $hotelcode);
                        })
                        ->select('htp_hotels.*', 'htp_images.original')
                        ->first();

                    if (is_object($htp)) {

                        if (!empty($htp->description)) {
                            $description = $htp->description;
                        }

                        $results = array(

                            "name" => $htp->hotel,
                            "description" => $description,
                            "code" => $htp->code,
                            "address" => $htp->address,
                            "country" => strtoupper($htp->country),
                            "latitude" => $htp->latitude,
                            "longitude" => $htp->longitude,
                            "stars" => $htp->stars,
                            "images" => $htp->original,
                            "rooms" => $rooms,
                            "source" => "htp"
                        );

                        array_push($response, $results);

                    }
                }
            }
        }
		return $response;
    }
	public function getTAPP($apiKey, $long, $lat, $country,$checkin, $checkout, $rooms, $pax){
		// get the country name, latitude, and longitude
		$public = public_path();

		// the json file is large, so we need to temporarily increase our allowed limit
		$endpoint = "http://ews.expedia.com/wsapi/rest/hotel/v1/search?location=".$lat.",".$long."&radius=10km&dates=" . $checkin . "," . $checkout . "&key=" . getenv('TAPP_SECRET') . "&deeplinks=ratedetails&format=JSON";
		$TAPP_KEY = "useragent=" . getenv('TAPP_KEY');
		$TAPP_SECRET = "expedia-apikey key=" . getenv('TAPP_SECRET');
		$headers = ["User-Agent" => $TAPP_KEY, "Authorization" => $TAPP_SECRET];

    	$client = new Client;
    	$request = new HTTP_Request('GET', $endpoint);

		$hotel = array();
		$responsedata = array();
		
		try 
		{
			$request = new HTTP_Request('GET', $endpoint);
			$response = $client->send($request);
            $body = $response->getBody();
			$hotels = json_decode($body, true);
			
			$i = 0;
			foreach ($hotels["HotelInfoList"]["HotelInfo"] as $hotel) {
				$images = array();
				if(!array_key_exists("StarRating", $hotel)){
					$hotel["StarRating"] = null;
				}
				// we need to get images
				$endpoint = "https://partnerapi.expedia.com/m/hotels/info?hotelId=". $hotel['HotelID'] . "&format=JSON";
				
				$response = $client->request('GET', $endpoint, [
				    'headers' => $headers
				]);
				$body = $response->getBody();
				$hotelData = json_decode($body, true);

				foreach ($hotelData as $key => $value) {
					$hoteldt[$key] = $value;
				}

				foreach ($hoteldt["photos"] as $image) {
					array_push($images, "https://images.trvl-media.com" . $image["url"]);
					break;
				}

				// end images get
				// get rooms

				$endpoint = "https://partnerapi.expedia.com/m/hotels/offers?hotelId=". $hotel['HotelID'] . "&checkInDate=$checkin&checkOutDate=$checkout&room=$rooms&format=JSON";
				
				$response = $client->request('GET', $endpoint, [
				    'headers' => $headers
				]);
				$body = $response->getBody();
				$hotelRoomResponse = json_decode($body, true);
				$roomsresponse = array("name" => array(), "price" => array(),"board" => array(),"key" => array(),"quantity" => array());
				
				if(!array_key_exists("errors", $hotelRoomResponse)){
				foreach ($hotelRoomResponse["hotelRoomResponse"] as $room) {
					array_push($roomsresponse["price"], $room["rateInfo"]["chargeableRateInfo"]["totalPriceWithMandatoryFees"]);
					array_push($roomsresponse["name"], $room["roomTypeDescription"]);
					array_push($roomsresponse["key"], $room["productKey"]);
					array_push($roomsresponse["board"], "NA");
					array_push($roomsresponse["quantity"], 1);
				}

				// end get rooms
				
				$results = array(
					"name" => $hotel["Name"],
					"description" => $hotel["Description"],
					"code" => $hotel["HotelID"],
					"address" => $hotel["Location"]["StreetAddress"] . ", " . $hotel["Location"]["City"] . ", " . $hotel["Location"]["Province"] . ", " . $hotel["Location"]["Country"],
					"country" => $hotel["Location"]["Country"],
					"latitude" => $hotel["Location"]["GeoLocation"]["Latitude"],
					"longitude" => $hotel["Location"]["GeoLocation"]["Longitude"],
					"stars" => (string) $hotel["StarRating"],
					"images" => $images,
					"rooms" => $roomsresponse,
					"source" => "tapp"
					);
					// we only want hotels with images
				array_push($responsedata, $results);
				}
				sleep(2);
				if(sizeof($responsedata) > 5){
					break;
				}
			}	
		} catch (Exception $ex) 
		{
		    printf("Error while sending request, reason: %s\n",$ex->getMessage());
		}
		return $responsedata;
	}
}
