<?php

namespace App\Http\Controllers;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;

class ChronController extends Controller
{
    public function getHTB()
    {
    	ini_set('memory_limit','99999M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
    	$responsedata = array();
    	$catch = array();

    	for($x = 1000; $x < 200001; $x+=1000){
    		array_push($catch, $x);
    	}

    	$client = new Client;
    	$apiKey =  "6dkxckesy858wcghpmdcw3sd";
        $sharedSecret = "FVfNRnnGwn";
        $signature = hash("sha256", $apiKey.$sharedSecret.time());
		$headers = ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" =>"application/JSON"];

    	for($i = 89001; $i <= 119000; $i+=1000)
    	{
    		$x = $i + 999;
    		echo "<br>Hotel Range: $i to $x <br> Loading...";
	    	$endpoint = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels?fields=all&from=$i&to=$x";
	    	
			$request = new request('GET', $endpoint, ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" => "application/JSON"]);
	        try 
	        {
	        	$response = $client->send($request);
	            $body = $response->getBody();
	           	$hotels = json_decode($body, true);  
	           	$hotel = [];

	           	foreach ($hotels["hotels"] as $key => $value) 
	           	{
	           		$hotel[$key] = $value;
	        	}
	        	
           		echo "<br>-------------------- COMPLETE ---------------- <br>" . count($hotel) . " Hotels Saved To /public/htb/hotels-$x.json <br>";
			    $fp = fopen(base_path(). "/public/htb/hotels-$x.json", 'w');
				fwrite($fp, json_encode($hotel));
				fclose($fp);
				unset($hotel);
	        }
	        catch (BadResponseException $e) 
	        {
	        	print_r($e->getResponse()->getBody()->getContents());
	        }	
	    }
    }

	public function getHTBHotels(){
		ini_set('memory_limit','5560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
    	$responsedata = array();
		$catch = array();
    	for($x = 5000; $x < 75001; $x+=1000){
    		array_push($catch, $x);
    	}
    	foreach ($catch as $range) {
    		$hotels = json_decode(file_get_contents(base_path(). "/public/htb/hotels-$range.json"), true);
    		foreach ($hotels as $hotel) {
    			if(array_key_exists("hotel", $hotel)){
    				$results = array("name" => array(), "code" => array());
    				$results["name"] = $hotel["hotel"]["name"]["content"];
    				$results["code"] = $hotel["hotel"]["code"];
    				array_push($responsedata, $results);
    				unset($results);
    			}
    		}
    	}
    	$c = json_encode($responsedata, 128);
    	$fp = fopen(base_path(). "/public/htb/mappingfile.json", 'w');
		fwrite($fp, $c);
	}

	public function combineHTB(){
		ini_set('memory_limit','10000M');
		ini_set('max_execution_time', 900000);
		set_time_limit(900000);
		$hotels = array();
		for($i = 100000; $i < 119000; $i+=10000){
			if(file_exists(base_path(). "/public/htb/hotels-htb-$i.json")){
				$f = file_get_contents(base_path(). "/public/htb/hotels-htb-$i.json");
				$info = json_decode($f, true);
				foreach ($info as $hotel) {
					array_push($hotels, $hotel);
				}
			}
		}
		usort($hotels, sorter('code'));
		$c = json_encode($hotels);
			$fp = fopen(base_path(). "/public/htb/hotels-htb-250000s.json", 'w');
			fwrite($fp, $c);
			fclose($fp);

			echo "done";
	}

	public function makelite(){
		ini_set('memory_limit','10000M');
		ini_set('max_execution_time', 900000);
		set_time_limit(900000);
		
		for($i = 250000; $i < 510000; $i+=50000){
			$hotels = array();
			if(file_exists(base_path(). "/public/htb/hotels-htb-$i" . "s.json")){
				$f = file_get_contents(base_path(). "/public/htb/hotels-htb-$i" . "s.json");
				$info = json_decode($f, true);
				foreach ($info as $hotel) {
					if(array_key_exists("images", $hotel)){

						foreach ($hotel["images"] as $image) 
						{
			            	if($image["order"] == "1"){
			            		$images = "https://photos.hotelbeds.com/giata/" . $image["path"];
			            		break;
			            	}
			            }
			        } else {
			        	$images = "https://membershiptravel.com/images/hotel.jpg";
			        }

		            if(array_key_exists("coordinates", $hotel)){
			            	$longitude = $hotel["coordinates"]["longitude"];
			            	$latitude = $hotel["coordinates"]["latitude"];
			            } else {
			            	$longitude = 0;
			            	$latitude = 0;
			        }
			        if(array_key_exists("description", $hotel)){
			            	$description = $hotel["description"]["content"];
			            } else {
			            	$description = "Description coming soon...";
			        }

					$results = array(
							"name" => $hotel["name"]["content"],
							"description" => $description,
							"code" => $hotel["code"],
							"country" => $hotel["countryCode"],
							"address" => $hotel["address"]["content"],
							"longitude" => $longitude,
							"latitude" => $latitude,
							"stars" => $hotel["categoryCode"][0],
							"images" => $images,
							"source" => "htb"
							);

					array_push($hotels, $results);
				}
			}
			
			$c = json_encode($hotels);
			$fp = fopen(base_path(). "/public/htb-lite2/hotels-htb-lite-$i". "s.json", 'w');
			fwrite($fp, $c);
			fclose($fp);
			unset($hotels);
			

			echo "<br>done";
		}
		
	}

	public function orderHTB(){
		ini_set('memory_limit','10000M');
		ini_set('max_execution_time', 900000);
		set_time_limit(900000);
		for($x = 26; $x < 101; $x++){
		$hotels[$x] = array();
		}

		for($i = 68000; $i < 119000; $i+=1000){
			if(file_exists(base_path(). "/public/htb/hotels-$i.json")){
				
				$f = file_get_contents(base_path(). "/public/htb/hotels-$i.json");
				$info = json_decode($f, true);

				foreach ($info as $hotel) {
					for($z = 250000; $z < 1010000; $z+=10000){
						$h = $z + 10000;
						if($hotel["code"] > $z && $hotel["code"] <= $h){
							$hit = $h / 10000;
							array_push($hotels[$hit], $hotel);
						}
					}
				}

				echo "<br> ---------I Count: $i-----------<br>";
				echo "<pre>";
				$hhh = 0;
				foreach ($hotels as $count) {
					$zzz = $hhh + 10000;
					echo "Num of Hotels between $hhh and $zzz: " . sizeof($count). "<br>";
					$hhh = $hhh + 10000;
				}
				echo "</pre><br>";
			}
		}

		for($x = 26; $x < 101; $x++){
			$xx = $x * 10000;
			usort($hotels[$x], sorter('code'));
			$c[$x] = json_encode($hotels[$x]);
			$fp[$x] = fopen(base_path(). "/public/htb/hotels-htb-$xx.json", 'w');
			fwrite($fp[$x], $c[$x]);
			fclose($fp[$x]);
		}
	}

	public function getHTBImageTypes(){
		$client = new Client;
    	$apiKey =  "6dkxckesy858wcghpmdcw3sd";
        $sharedSecret = "FVfNRnnGwn";
        $signature = hash("sha256", $apiKey.$sharedSecret.time());
		$headers = ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" =>"application/JSON"];

		$endpoint ="https://api.hotelbeds.com/hotel-content-api/1.0/types/imagetypes?lastUpdateTime=2015-09-10&fields=all&language=ENG";
		$request = new request('GET', $endpoint, ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" => "application/JSON"]);
		 try 
	        {
	        	$response = $client->send($request);
	            $body = $response->getBody();
	           	$images = json_decode($body, true);  
	           	$image = [];
	           	foreach ($images as $key => $value) {
	           		$image[$key] = $value;
	           	}
	           	
	           	echo json_encode($image["imageTypes"]);
	           	
	        }
	        catch (BadResponseException $e) 
	        {
	        	print_r($e->getResponse()->getBody()->getContents());
	        }	
	}

	public function getHTP(){
		ini_set('memory_limit','10000M');
		ini_set('max_execution_time', 900000);
		set_time_limit(900000);
		
		

		for($i = 0; $i <= 33; $i++){
			for($z = 1; $z <= 14; $z++){
				$hoteldata = array();
				$responsedata = array();
				if(file_exists(base_path(). "/public/htp/hotels-$i-$z.json")){
					$hotels = json_decode(file_get_contents(base_path(). "/public/htp/hotels-$i-$z.json"), true);
					foreach ($hotels as $hoteldata) {
						$images = array();
						if(array_key_exists("name", $hoteldata)){
							$name = $hoteldata["name"];
						}
						if(array_key_exists("stars", $hoteldata)){
							$stars = $hoteldata["stars"];
						}
						if(array_key_exists("images", $hoteldata)){
				   			foreach ((array)$hoteldata["images"] as $image) {
								array_push($images, $image["original"]);
								break;
							}
						}
						$description = "";
						if(array_key_exists("descriptions", $hoteldata)){
							if(array_key_exists("hotel_information", (array)$hoteldata["descriptions"])){
								$description = $hoteldata["descriptions"]["hotel_information"];
							}
						}
						if(!empty($name) && !empty($images)){
							$result = array(
			                	"name" => $name,
			                	"code" => $hoteldata["code"],
			                	"address" => $hoteldata["address"] . ", " . strtoupper($hoteldata["country"]),
			                	"longitude" => $hoteldata["longitude"],
			                	"latitude" => $hoteldata["latitude"],
			                	"description" => $description,
			                	"images" => $images,
			                	"stars" => $stars
			                	);

							array_push($responsedata, $result);
							unset($result);
						}
					}
					$c = json_encode($responsedata, 128);
			    	$fp = fopen(base_path(). "/public/htp-lite2/hotels-$i-$z.json", 'w');
					fwrite($fp, $c);
					echo ".";
				}
			}
		}

	} 
}

