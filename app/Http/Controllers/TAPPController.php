<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Illuminate\Http\Request;
use DB;

class TAPPController extends Controller
{

	public function getHotel($apiKey, $code){
		$hotel = [];
		$responsedata = array();
		$images = array(); 
		$images["name"] = array();
		$images["path"] = array();
		$rooms = array(); 
		$rooms["code"] = array();
		$rooms["description"] = array();
		$facilities = array();
		$facilities["payments"] = array();
		$facilities["room_amenities"] = array();
		$facilities["hotel_amenities"] = array();  

		$client = new Client;

		$endpoint = "https://partnerapi.expedia.com/m/hotels/info?hotelId=$code&format=JSON";
		$TAPP_KEY = "useragent=" . getenv('TAPP_KEY');
		$TAPP_SECRET = "expedia-apikey key=" . getenv('TAPP_SECRET');
		$headers = ["User-Agent" => $TAPP_KEY, "Authorization" => $TAPP_SECRET];
		
		$response = $client->request('GET', $endpoint, ['headers' => $headers]);
		$body = $response->getBody();
		$hotelData = json_decode($body, true);

		foreach ($hotelData as $key => $value) {
			$hotel[$key] = $value;
		}
		// we must get all images

		foreach ($hotel["photos"] as $image) {
			array_push($images["path"], "https://images.trvl-media.com" . $image["url"]);
	        array_push($images["name"], $image["displayText"]);
		}

		// end get images

		// get all facilities

		foreach ($hotel["hotelAmenities"] as $facility) {
        		array_push($facilities["hotel_amenities"], $facility["description"]);
        }

		// end get facilities
		

		$result = array(
                	"name" => $hotel["hotelName"],
                	"address" => $hotel["hotelAddress"],
                	"city" => $hotel["hotelCity"],
                	"country" => $hotel["hotelCountry"],
                	"description" => $hotel["longDescription"],
                	"phonenumbers" => null,
                	"rooms" => null,
                	"images" => $images,
                	"facilities" => $facilities,
                	"stars" => $hotel["hotelStarRating"]
                	);
		echo json_encode($result, 128);
	}
}
