<?php

namespace App\Http\Controllers;

require base_path() .'/vendor/autoload.php';

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;


use hotelbeds\hotel_api_sdk\HotelApiClient;
use hotelbeds\hotel_api_sdk\messages\BookingConfirmRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRS;
use hotelbeds\hotel_api_sdk\messages\CheckRateRQ;
use hotelbeds\hotel_api_sdk\model\Geolocation;
use hotelbeds\hotel_api_sdk\model\Hotels;
use hotelbeds\hotel_api_sdk\model\Destination;
use hotelbeds\hotel_api_sdk\model\Occupancy;
use hotelbeds\hotel_api_sdk\model\Pax;
use hotelbeds\hotel_api_sdk\model\Rate;
use hotelbeds\hotel_api_sdk\model\Stay;
use hotelbeds\hotel_api_sdk\types\ApiVersion;
use hotelbeds\hotel_api_sdk\types\ApiVersions;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRS;
use hotelbeds\hotel_api_sdk\messages\AvailabilityRQ;
use hotelbeds\hotel_api_sdk\messages\BookingListRS;
use hotelbeds\hotel_api_sdk\messages\BookingCancellationRS;

// getHotelFromDB

use App\HTBHotels;
use App\HTBHotelImages;
use App\HTBImageTypes;
use App\HTBHotelFacilities;

class HotelbedsController extends Controller
{
	/**
     * @var HotelApiClient
     */

	private $apiClient;
	private $pax;
	private $occupancy;
	
	public function index($apiKey, $long, $lat, $country, $checkin, $checkout, $rooms, $pax){

		$responsedata = array();
		$client = new Client;
		$public = public_path();
		ini_set('memory_limit','2560M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
		$hotelpath = base_path(). "/vendor/hotelbeds/hotel-api-sdk-php/tests/";
    	$reader = new Zend\Config\Reader\Ini();
		$config   = $reader->fromFile($hotelpath . 'HotelApiClient.ini');
		$cfgApi = $config["apiclient"];
		$hotel = [];
		

		 $this->apiClient = new HotelApiClient(getenv('HTB_URL'),
	            getenv('HTB_KEY'),
	            getenv('HTB_SECRET'),
            new ApiVersion(ApiVersions::V1_0), "120" );

		$availRS = $this->testAvailRQ($long, $lat, $checkin, $checkout, $rooms, $pax);
		$firstRate = $this->testAvailRS($availRS);
		$hotelrate = $availRS->hotels->toArray()["hotels"];
		$rooms = array("name" => array(), "price" => array(), "board" => array(), "key" => array(), "quantity" => array());
		foreach ($hotelrate as $hotel) {
			
			foreach ($hotel["rooms"] as $room) {
		   		array_push($rooms["quantity"], $room["rates"]["0"]["rooms"]);
		   		array_push($rooms["name"], $room["name"]);
		   		array_push($rooms["price"], $room["rates"]["0"]["net"]);
		   		array_push($rooms["board"], $room["rates"]["0"]["boardCode"]);
		   		array_push($rooms["key"], $room["rates"]["0"]["rateKey"]);
			}

		   	$hotelcode = $hotel["code"];

		   	$path = $public . "/htb-lite/" . getHTBLite($hotelcode);
		   	$db = file_get_contents($path);
		   	$hotelrepo = json_decode($db, true);
		   
		   	foreach ((array)$hotelrepo as $hotelrep) {

		   	//	if(array_key_exists("hotel", $hotelrep)){
		   			if(array_key_exists("code", $hotelrep)){	
		
		   				if($hotelrep["code"] == $hotelcode){
		   					//echo $hotelrep["hotel"]["code"] . "-" . $hotelcode;
				        	

				            $results = array(
							"name" => $hotelrep["name"],
							"description" => $hotelrep["description"],
							"code" => $hotelcode,
							"country" => $hotelrep["country"],
							"latitude" => $hotelrep["latitude"],
							"longitude" => $hotelrep["longitude"],
							"stars" => $hotelrep["stars"],
							"images" => $hotelrep["images"],
							"rooms" => $rooms,
							"source" => "htb"
							);

				            array_push($responsedata, $results);
							$images = array();
							$rooms = array("name" => array(), "price" => array(), "board" => array(), "key" => array(), "quantity" => array());
				   		}
		   			}
		   	//	}
		   }
		   
		}
		echo json_encode($responsedata);

		/*
		$checkRS = $this->testCheckRate($firstRate);
		$booking = $this->testBookingConfirm($checkRS);
		echo "<pre>";
		print_r($booking);
		echo "</pre>";
		*/
	}

	public function testAvailRQ($long, $lat, $checkin, $checkout, $rooms, $pax)
    {
        $rqData = new \hotelbeds\hotel_api_sdk\helpers\Availability();
		$rqData->stay = new Stay(DateTime::createFromFormat("Y-m-d", $checkin),
		                         DateTime::createFromFormat("Y-m-d", $checkout));

        //$rqData->destination = new Destination("PMI");
        $geolocation = new Geolocation();
        $geolocation->latitude = (double)$lat;
        $geolocation->longitude= (double)$long;
        $geolocation->radius= 24.0;
        $geolocation->unit= Geolocation::KM;

        $rqData->geolocation = $geolocation;

        $children = paxDecode($pax)["children"];
        $adults = paxDecode($pax)["adults"];
        $ages = paxDecode($pax)["ages"];
            
        $occupancy = new Occupancy();
        $occupancy->adults = (int)$adults;
        $occupancy->children = (int)$children;
        $occupancy->rooms = (int)$rooms;

        $guests=array_fill(0, $occupancy->adults+$occupancy->children, NULL);
		$pos=0;
		for ($x=0;$x<$occupancy->adults; $x++){
		    $guests[$pos]= new Pax(Pax::AD, 30);
		    $pos++;
		}
		for ($y=0;$y<$occupancy->children; $y++) {
		    $guests[$pos]= new Pax(Pax::CH, $ages[$y]);
		    $pos++;
		}
		$occupancy->paxes =$guests;
		$rqData->occupancies = [ $occupancy ];

        return $this->apiClient->Availability($rqData);
    }

        public function testAvailRS($availRS)
    {
        // Check is response is empty or not

        // Check some fields of response
        // Iterate response hotels, rooms, rates...
        if(!empty($availRS)){
	        foreach ($availRS->hotels->iterator() as $hotelCode => $hotelData)
	        {
	            $hotelData->name;

	            foreach ($hotelData->iterator() as $roomCode => $roomData)
	            {
	                $roomData->code;

	                foreach($roomData->rateIterator() as $rateKey => $rateData)
	                {
	                    $firstRate = $rateData;

	                    $rateData->net;
	                    $rateData->allotment;
	                    $rateData->boardCode;

	                    // Check cancellation policies
	                    foreach($rateData->cancellationPoliciesIterator() as $policyKey => $policyData)
	                    {
	                        $policyData->amount;
	                        $policyData->from;
	                    }

	                    // Check taxes
	                    $taxes = $rateData->getTaxes();
	                    foreach($taxes->iterator() as $tax)
	                    {
	                            //print_r($tax);
	                            //$availRS->tax->type);
	                    }

	                    // Promotions
	                    foreach($rateData->promotionsIterator() as $promoCode => $promoData )
	                    {
	                        $promoData->name;
	                    }
	                }

	            }
	        }
    	}

        return $firstRate;
    }

    public function testCheckRate($firstRate)
    {
        $rqCheck = new \hotelbeds\hotel_api_sdk\helpers\CheckRate();
        $rqCheck->rooms = [ ["rateKey" => $firstRate->rateKey] ];

        return $this->apiClient->checkRate($rqCheck);
    }

        /**
     * @depends testCheckRate
     */

    public function testBookingConfirm($checkRS)
    {
           $rqBookingConfirm = new \hotelbeds\hotel_api_sdk\helpers\Booking();
           $rqBookingConfirm->holder = new \hotelbeds\hotel_api_sdk\model\Holder("Tomeu TEST", "Capo TEST");
           $rqBookingConfirm->language="CAS";

           // Use this iterator for multiple pax distributions, this example have one only pax distribution.

           $paxes = [ new Pax(Pax::AD, 30, "Miquel", "Fiol", 1), new Pax(Pax::AD, 27, "Margalida", "Soberats", 1), new Pax(Pax::CH, 8, "Josep", "Fiol", 1) ];
           $bookRooms = [];
           $atWeb = false;
           foreach ($checkRS->hotel->iterator() as $roomCode => $roomData)
           {
               if ($roomData->rates[0]["rateType"] !== "BOOKABLE")
                   continue;

               $bookingRoom = new \hotelbeds\hotel_api_sdk\model\BookingRoom($roomData->rates[0]["rateKey"]);
               $bookingRoom->paxes = $paxes;
               $bookRooms[] = $bookingRoom;

               $atWeb = ($roomData->rates[0]["paymentType"] === "AT_WEB");
           }

           // Check all bookable rooms are inserted for confirmation.

           $rqBookingConfirm->rooms = $bookRooms;

           // Define payment data for booking confirmation
           $rqBookingConfirm->clientReference = "PHP_TEST_2";
           if (!$atWeb) {
               $rqBookingConfirm->paymentData = new \hotelbeds\hotel_api_sdk\model\PaymentData();

               $rqBookingConfirm->paymentData->paymentCard = [
                   "cardType" => "VI",
                   "cardNumber" => "4444333322221111",
                   "cardHolderName" => "AUTHORISED",
                   "expiryDate" => "0620",
                   "cardCVC" => "123"
               ];

               $rqBookingConfirm->paymentData->contactData = [
                   "email" => "integration@test.com",
                   "phoneNumber" => "654654654"
               ];
           }

           try {
               $confirmRS = $this->apiClient->BookingConfirm($rqBookingConfirm);
               return $confirmRS;
           } catch (\hotelbeds\hotel_api_sdk\types\HotelSDKException $e) {
               echo "\n".$e->getMessage()."\n";
               echo "\n".$this->apiClient->getLastRequest()->getContent()."\n";
               $this->fail($e->getMessage());
           }

           return null;
    }
    public function getHotel($apiKey, $code, $long){
		$fee = "";
		$images = array(); 
		$images["name"] = array();
		$images["path"] = array();
		$rooms = array(); 
		$rooms["code"] = array();
		$rooms["description"] = array();
		$facilities = array();
		$facilities["payments"] = array();
		$facilities["room_amenities"] = array();
		$facilities["hotel_amenities"] = array();

        $hotel = HTBHotels::where('code',$code)->first();
        $htbImage = HTBHotelImages::where('hotel',$code)->get();
        $htbFacilities = HTBHotelFacilities::where('hotel',$code)->get();


        if(sizeof($htbImage) > 0){
            foreach ($htbImage as $image) {
                    $htbImageType = HTBImageTypes::where('type', $image->name)->first();
                    array_push($images["path"], "https://photos.hotelbeds.com/giata/bigger/" . $image->path);
                    array_push($images["name"], $htbImageType->name);
            }
    	}

        	// we return the name and categories of all amenities

            foreach ($htbFacilities as $facility) {

                if($facility->indFee == 1){
                    $fee = " (has fee)";
                }

                if($facility->facilityGroupCode == 30){
                    array_push($facilities["payments"], $facility->facility);
                }
                if($facility->facilityGroupCode == 60){
                    array_push($facilities["room_amenities"], $facility->facility . $fee);
                }
                if($facility->facilityGroupCode == 70 || $facility->facilityGroupCode == 71){
                    array_push($facilities["hotel_amenities"], $facility->facility . $fee);
                }
            }

        	// we return all available rooms

            if(array_key_exists("rooms", $hotel)) {
                foreach ($hotel["rooms"] as $room) {
                    array_push($rooms["code"], $room["roomCode"]);
                    if (array_key_exists("roomStays", $room)) {
                        array_push($rooms["description"], $room["roomStays"][0]["description"]);
                    } else {
                        array_push($rooms["description"], "");
                    }
                }
            }

            if(isset($hotel->phones)){
                $phones = $hotel->phones;
            } else {
                $phones = array();
            }

            $results = array(
            	"name" => $hotel->hotel,
            	"address" => $hotel->address,
            	"city" => $hotel->city,
            	"country" => $hotel->country,
            	"description" => $hotel->description,
            	"phonenumbers" => array(),
            	"rooms" => array(),
            	"images" => $images,
            	"facilities" => $facilities,
            	"stars" => $hotel->stars
            	);

            echo json_encode($results, 128);
            
	}
	public function cancelReservation($apiKey, $confirm_num){
		$client = new Client;
           $endpoint = getenv('HTB_URL') . "/hotel-api/1.0/bookings/$confirm_num?cancenllationFlag=SIMULATION";
	    	$apiKey =  getenv('HTB_KEY');
	        $sharedSecret = getenv('HTB_SECRET');
	        $signature = hash("sha256", $apiKey.$sharedSecret.time());
			$headers = ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" =>"application/JSON"];
			$request = new HTTP_Request('DELETE', $endpoint, ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" => "application/JSON"]);
	        try 
	        {
	        	$response = $client->send($request);
	            $body = $response->getBody();
	           	echo $body;
	           	
	        }
	        catch (BadResponseException $e) 
	        {
	        	print_r($e->getResponse()->getBody()->getContents());
	        }	
    }
}
