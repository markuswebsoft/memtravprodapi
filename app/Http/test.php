$images = array();
				foreach ($hotel["images"] as $image) {
					array_push($images, $image["original"]);
				}
				$results = array(
					"name" => $hotel["name"],
					"description" => $hotel["descriptions"]["hotel_information"],
					"code" => $hotel["code"],
					"country" => $hotel["country"],
					"latitude" => $hotel["latitude"],
					"longitude" => $hotel["longitude"],
					"stars" => $hotel["stars"],
					"images" => $images
					);
				print_r($results);
				array_push($response, $results);






					// this is the final output
					$results = array(
						"name" => $hotel["name"],
						"description" => $hotel["descriptions"]["hotel_information"],
						"code" => $hotel["code"],
						"country" => $hotel["country"],
						"latitude" => $hotel["latitude"],
						"longitude" => $hotel["longitude"],
						"stars" => $hotel["stars"],
						"images" => $images
						);
					// build the response array with each successful query
					array_push($response, $results);


					/// 

					if($apikey == "ieAIfHyiUGyQBAi9ktu6w5tYuItBdG9Fk8aFrxHp"){
		
		$public = public_path();

		// the json file is large, so we need to temporarily increase our allowed limit
		ini_set('memory_limit','2560M');

		// this will get all of the hotels into a workable JSON database
		$hotelsDB = file_get_contents("$public/db/hotels.json");
		$hotels = json_decode($hotelsDB, true); 


		// we cannot search for hotels unless we get the destination code
		// we will get the destination code that matches the query
		for($i = 0; $i < 81; $i++){
			$destinationsDB = file_get_contents("$public/db/destinations-$i.json");
			$destinations = json_decode($destinationsDB, true);
			foreach ($destinations as $destination) {
				if($destination["country"] == $country && $destination["name"] == $dest){
					// we found a match!
					$desCode = $destination["code"];
					// end the loop to conserve memory
					break;
				}
			}
				if(!empty($desCode)){
				foreach ($destinations as $destination) {
					if($destination["parent"] == $desCode){
						// we found a match!
						$desChild = array("desChild" => $destination["code"]);
						array_push($children, $desChild);
					}
				}
			}
		}

		// once a match is found, we now find all of the hotels matching the destination code

		if(!empty($desCode)){
			foreach ($hotels as $hotel) {
				foreach($children as $child){
					if($hotel["destination"] == $desCode || $hotel["destination"] == $child["desChild"]){
						// hotels have more than one image, so we build an array of the images first
						$images = array();
						$x = 1;
						foreach ((array)$hotel["images"] as $image) {
							array_push($images, $image["original"]);
							if($x == 1){
								// we only need 3 images 
								break;
							}
							$x++;
						}
						// not all hotels have descriptions in the same spot, so we will use if statements to get it out
							if (array_key_exists('hotel_information', (array)$hotel["descriptions"])) {
								$description = $hotel["descriptions"]["hotel_information"];
							} else {
								$description = $hotel["descriptions"]["location_information"];
							}

						// this is the final output
						$results = array(
							"name" => $hotel["name"],
							"description" => $description,
							"code" => $hotel["code"],
							"country" => $hotel["country"],
							"latitude" => $hotel["latitude"],
							"longitude" => $hotel["longitude"],
							"stars" => $hotel["stars"],
							"images" => $images
							);
						// build the response array with each successful query
						array_push($response, $results);
					}
				}
			}
		}

		// echo json_encode($response, 128);
		echo json_encode($response, 128);
	} else {
		echo "<h1>Not Authorized</h1>";
	}