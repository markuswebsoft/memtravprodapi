<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use App\Countries;
use DB;
use Zend;
use DateTime;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;
use App\HTBHotels as Hotels;
use App\HTBHotelImages as Images;
use App\HTBHotelFacilities as Facilities;

class Cron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $signature = 'cron:htb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab all hotels from HotelBeds';

	public function handle()
    {
    	// grab all hotels from hotelbeds
        $this->grabHTB();
		
		// organize the files from hotel id batches
		//$this->grabHTP();
    }
	public function grabHTB(){
		ini_set('memory_limit','99999M');
		ini_set('max_execution_time', 90000);
		set_time_limit(90000);
		$client = new Client;
    	$apiKey =  "6dkxckesy858wcghpmdcw3sd";
        $sharedSecret = "FVfNRnnGwn";

    	for($i = 1; $i <= 700000; $i+=1)
    	{
    		$signature = hash("sha256", $apiKey.$sharedSecret.time());
			$headers = ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" =>"application/JSON"];
	    	$endpoint = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels/$i?language=ENG";
			$request = new request('GET', $endpoint, ["Api-Key" => $apiKey, "X-Signature" => $signature, "Accept" => "application/JSON"]);
	        try 
	        {
	        	$response = $client->send($request);
	            $body = $response->getBody();
	           	$hotelresponse = json_decode($body, true);
				$hotel = [];
				if(array_key_exists("hotel", $hotelresponse)){
		           	foreach ($hotelresponse["hotel"] as $key => $value) 
		           	{
		           		$hotel[$key] = $value;
		        	}
					
					if(array_key_exists("code", $hotel)){
					    $code = $hotel["code"];
						echo "\n".$code. "\n";
                        // does this hotel already exist?
                        $count = Hotels::where('code', $code)->count();

                        // establish simple hotel vars

                        if (array_key_exists("description", $hotel)) {
                            $description = $hotel["description"]["content"];
                        } else {
                            $description = NULL;
                        }
                        if (array_key_exists("postalCode", $hotel)) {
                            $postalCode = $hotel["postalCode"];
                        } else {
                            $postalCode = NULL;
                        }
                        if (array_key_exists("coordinates", $hotel)) {
                            $longitude = $hotel["coordinates"]["longitude"];
                            $latitude = $hotel["coordinates"]["latitude"];
                        } else {
                            $longitude = 0;
                            $latitude = 0;
                        }
                        if (array_key_exists("country", $hotel)) {
                            $country = $hotel["country"]["code"];
                        } else {
                            $country = "";
                        }
                        if (array_key_exists("category", $hotel)) {
                            $category = $hotel["category"]["description"]["content"];
                        } else {
                            $category = "";
                        }
                        if (array_key_exists("email", $hotel)) {
                            $email = $hotel["email"];
                        } else {
                            $email = NULL;
                        }

                        // end vars

                        // if yes
                        if($count > 0){
                            // update hotel property info

                        } else {
                            // this hotel is new

                            DB::table('htb_hotels')->insert([
                                'code' => $hotel["code"],
                                'hotel' => $hotel["name"]["content"],
                                'description' => $description,
                                'country' => $country,
                                'longitude' => $longitude,
                                'latitude' => $latitude,
                                'stars' => $category,
                                'address' => $hotel["address"]["content"],
                                'city' => $hotel["city"]["content"],
                                'postalCode' => $postalCode
                            ]);
                            // insert email
                            DB::table('htb_phones')->insert([
                                'hotel' => $hotel["code"],
                                'phoneNumber' => $email,
                                'phoneType' => "email"
                            ]);
                            if (array_key_exists("phones", $hotel)) {
                                foreach ($hotel["phones"] as $phone) {
                                    DB::table('htb_phones')->insert([
                                        'hotel' => $hotel["code"],
                                        'phoneNumber' => $phone["phoneNumber"],
                                        'phoneType' => $phone["phoneType"]
                                    ]);
                                }
                            }
                            if (array_key_exists("boards", $hotel)) {
                                foreach ($hotel["boards"] as $board) {
                                    DB::table('htb_board')->insert([
                                        'hotel' => $hotel["code"],
                                        'board' => $board["code"]
                                    ]);
                                }
                            }
                            if (array_key_exists("rooms", $hotel)) {

                                foreach ($hotel["rooms"] as $room) {
                                    if(array_key_exists("description", $room)){
                                        $roomDescription = $room["description"];
                                    } else {
                                        $roomDescription = NULL;
                                    }
                                    DB::table('htb_rooms')->insert([
                                        'hotel' => $hotel["code"],
                                        'roomCode' => $room["roomCode"],
                                        'description' => $roomDescription
                                    ]);
                                }
                            }

                            // facilities

                            if (array_key_exists("facilities", $hotel)) {
                                $facilities = $hotel["facilities"];
                                foreach ($facilities as $facility) {
                                    if (array_key_exists("description", $facility)) {

                                        if(array_key_exists("order", $facility)){
                                            $facilityOrder = $facility["order"];
                                        } else {
                                            $facilityOrder = null;
                                        }

                                        if(array_key_exists("facilityCode", $facility)){
                                            $facilityCode = $facility["facilityCode"];
                                        } else {
                                            $facilityCode = null;
                                        }

                                        if(array_key_exists("facilityGroupCode", $facility)){
                                            $facilityGroupCode = $facility["facilityGroupCode"];
                                        } else {
                                            $facilityGroupCode = null;
                                        }

                                        if(array_key_exists("indFee", $facility)){
                                            $indFee = $facility["indFee"];
                                        } else {
                                            $indFee = null;
                                        }

                                        if(array_key_exists("indYesOrNo", $facility)){
                                            $indYesOrNo = $facility["indYesOrNo"];
                                        } else {
                                            $indYesOrNo = null;
                                        }


                                        DB::table('htb_hotel_facilities')->insert([
                                            'hotel' => $hotel["code"],
                                            'order' => $facilityOrder,
                                            'facilityCode' => $facilityCode,
                                            'facilityGroupCode' => $facilityGroupCode,
                                            'facility' => $facility["description"]["content"],
                                            'indFee' => $indFee,
                                            'indYesOrNo' => $indYesOrNo
                                        ]);
                                    }
                                }
                            }

                            if(array_key_exists("images", $hotel)){
                                $images = $hotel["images"];
                                foreach($images as $image) {
                                    if(array_key_exists("type", $image)) {
                                        if (array_key_exists("description", $image["type"])) {
                                            $imagename = $image["type"]["description"]["content"];
                                        } else {
                                            $imagename = NULL;
                                        }
                                    } else {
                                        $imagename = NULL;
                                    }

                                    DB::table('htb_hotel_images')->insert([
                                        'hotel' => $hotel["code"],
                                        'order' => $image["order"],
                                        'path' => "https://photos.hotelbeds.com/giata/" . $image["path"],
                                        'name' => $imagename
                                    ]);

                                }
                            }
                        }
					}
				}
	        }
	        catch (BadResponseException $e) 
	        {
	        	print_r($e->getResponse()->getBody()->getContents());
	        }
	    }
	}
	public function grabHTP(){
		
		echo "beginning download....\n";
		$url = "https://assets.cosmos-data.com/exports/consumer-2016.06.28-export.tar.bz2";
		$zipFile = base_path(). "/public/htp/hotels.tar.bz2"; 
		//file_put_contents($zipFile, fopen($url, 'r'));
		echo "download complete..\n saving file...\n";
		echo "now decompressing";
		//$p = new PharData($zipFile);
		//$p->decompress(); // creates /path/to/my.tar
	}
}
