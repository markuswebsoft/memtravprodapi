<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTBHotelFacilities extends Model
{
    protected $table = 'htb_hotel_facilities';
}