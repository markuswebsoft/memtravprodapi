<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTBImageTypes extends Model
{
    protected $table = 'htb_imagetypes';
}