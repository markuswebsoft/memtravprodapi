<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTPHotelFacilityType extends Model
{
    protected $table = 'htp_facility_types';
}