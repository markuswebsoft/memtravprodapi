<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HTPHotelFacilities extends Model
{
    protected $table = 'htp_facilities';
}